/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller.mapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.hearthstone.deck.string.controller.model.DeckControllerModel;
import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Deck.DeckBuilder;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;
import com.hearthstone.deck.string.service.CardEnrichmentService;
import com.hearthstone.deck.string.service.CardProducerService;

/**
 * Handler for {@link DeckMapper}
 */
public class DeckMapperHandler implements DeckMapper {

    /** The card enrichment service. */
    private final CardEnrichmentService cardEnrichmentService;

    /** The card producer service. */
    private final CardProducerService cardProducerService;

    /**
     * Instantiates a new deck mapper handler.
     *
     * @param cardMapper
     *            the card mapper
     */
    @Inject
    public DeckMapperHandler(final CardEnrichmentService cardEnrichmentService,
            final CardProducerService cardProducerService) {
        this.cardEnrichmentService = cardEnrichmentService;
        this.cardProducerService = cardProducerService;
    }

    /**
     * @InheritDoc
     */
    @Override
    public Deck mapTo(final DeckControllerModel deckControllerModel) {

        final DeckBuilder deckBuilder = new DeckBuilder();
        deckBuilder.withStartingHero(StartingHero.findValueFromClass(deckControllerModel.getStartingHero()));
        deckBuilder.withFormat(Format.findValueFromName(deckControllerModel.getFormat()));

        final Card[] cards = cardProducerService.produceCards();

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer oneOfCard : deckControllerModel.getOneOfCards()) {
            final Card card = cardEnrichmentService.findCardInJson(oneOfCard, cards);
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer twoOfCard : deckControllerModel.getTwoOfCards()) {
            final Card card = cardEnrichmentService.findCardInJson(twoOfCard, cards);
            twoOfCards.add(card);
        }

        deckBuilder.withOneOfCards(oneOfCards);
        deckBuilder.withTwoOfCards(twoOfCards);

        return deckBuilder.build();
    }

    /**
     * @InheritDoc
     */
    @Override
    public DeckControllerModel mapFrom(final Deck deck) {

        final DeckControllerModel deckControllerModel = new DeckControllerModel();
        deckControllerModel.setStartingHero(deck.getStartingHero().getHeroClass());
        deckControllerModel.setFormat(deck.getFormat().getFormatName());

        final List<Integer> oneOfCards = new ArrayList<Integer>();
        for (final Card card : deck.getOneOfCards()) {
            final Integer cardToAdd = card.getDbfId();
            oneOfCards.add(cardToAdd);
        }

        final List<Integer> twoOfCards = new ArrayList<Integer>();
        for (final Card card : deck.getTwoOfCards()) {
            final Integer cardToAdd = card.getDbfId();
            twoOfCards.add(cardToAdd);
        }

        deckControllerModel.setOneOfCards(oneOfCards);
        deckControllerModel.setTwoOfCards(twoOfCards);

        return deckControllerModel;
    }
}
