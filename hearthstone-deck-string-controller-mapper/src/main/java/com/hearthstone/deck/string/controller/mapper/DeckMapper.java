/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller.mapper;

import com.hearthstone.deck.string.controller.model.DeckControllerModel;
import com.hearthstone.deck.string.domain.Deck;

/**
 * Deck mapper, turns DeckControllerModel models into Deck models and vice
 * versa.
 */
public interface DeckMapper {

    /**
     * Map a DeckControllerModel into a Deck Model
     *
     * @param DeckControllerModel
     *            the Deck controller model
     * @return the Deck
     */
    public Deck mapTo(final DeckControllerModel deckControllerModel);

    /**
     * Map from a Deck model to DeckControllerModel
     *
     * @param Deck
     *            the Deck
     * @return the Deck controller model
     */
    public DeckControllerModel mapFrom(Deck deck);

}
