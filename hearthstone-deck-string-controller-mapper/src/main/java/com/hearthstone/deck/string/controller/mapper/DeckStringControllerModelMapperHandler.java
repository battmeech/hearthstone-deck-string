/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller.mapper;

import com.hearthstone.deck.string.controller.model.DeckStringControllerModel;

/**
 * Handler for {@link DeckStringControllerModelMapper}
 */
public class DeckStringControllerModelMapperHandler implements DeckStringControllerModelMapper {

    /**
     * @InheritDoc
     */
    @Override
    public DeckStringControllerModel mapTo(final String deckString) {
        final DeckStringControllerModel deckStringControllerModel = new DeckStringControllerModel();
        deckStringControllerModel.setDeckString(deckString);
        return deckStringControllerModel;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String mapFrom(final DeckStringControllerModel deckStringControllerModel) {
        return deckStringControllerModel.getDeckString();
    }

}
