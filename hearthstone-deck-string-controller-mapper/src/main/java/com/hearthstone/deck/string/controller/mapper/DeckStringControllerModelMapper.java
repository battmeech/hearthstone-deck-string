/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller.mapper;

import com.hearthstone.deck.string.controller.model.DeckStringControllerModel;

/**
 * Deck String Respone Model Mapper
 */
public interface DeckStringControllerModelMapper {

    /**
     * Take in the deck, deck string and validation result and create a response for
     * the consumer. The validation result will be used when the deck is too small.
     *
     * @param deck
     *            the deck
     * @param deckString
     *            the deck string
     * @param validationResult
     *            the validation result
     * @return the deck string response model
     */
    public DeckStringControllerModel mapTo(final String deckString);

    /**
     * Take in a deck string controller model and extract the deck string to be
     * converted into a deck object later on.
     *
     * @param deckStringControllerModel
     *            the deck string controller model
     * @return the string
     */
    public String mapFrom(final DeckStringControllerModel deckStringControllerModel);

}
