/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller.mapper.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.hearthstone.deck.string.controller.mapper.DeckMapper;
import com.hearthstone.deck.string.controller.mapper.DeckMapperHandler;
import com.hearthstone.deck.string.controller.model.DeckControllerModel;
import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Card.CardBuilder;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Deck.DeckBuilder;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;
import com.hearthstone.deck.string.service.CardEnrichmentService;
import com.hearthstone.deck.string.service.CardProducerService;

/**
 * Unit test for {@link DeckMapper}
 */
@RunWith(MockitoJUnitRunner.class)
public class DeckMapperTest {

    /** The card enrichment service. */
    @Mock
    private CardEnrichmentService cardEnrichmentService;

    /** The card producer service. */
    @Mock
    private CardProducerService cardProducerService;

    /** Class under test. */
    private DeckMapper deckMapper;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        deckMapper = new DeckMapperHandler(cardEnrichmentService, cardProducerService);
    }

    /**
     * Test the map to method.
     */
    @Test
    public void testMapTo() {

        // Setup expected deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419, 333);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927, 43502, 123);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (int index = 0; index < oneOfIds.size(); index++) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(5)
                    .withCardClass(StartingHero.JAINA.getHeroClass());
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (int index = 0; index < twoOfIds.size(); index++) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(5)
                    .withCardClass(StartingHero.JAINA.getHeroClass());
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final CardBuilder cardBuilder = new CardBuilder().withDbfId(5).withCardClass(StartingHero.JAINA.getHeroClass());
        final Card card = cardBuilder.build();

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck expectedDeck = deckBuilder.build();

        // Setup deck to run test on.
        final DeckControllerModel deckToTest = new DeckControllerModel();
        deckToTest.setFormat(format.getFormatName());
        deckToTest.setStartingHero(hero.getHeroClass());
        deckToTest.setOneOfCards(oneOfIds);
        deckToTest.setTwoOfCards(twoOfIds);

        // Mock
        Mockito.when(cardEnrichmentService.findCardInJson(Mockito.anyInt(), Mockito.anyObject())).thenReturn(card);

        // Run test
        final Deck actualDeck = deckMapper.mapTo(deckToTest);

        // Assert
        Assert.assertEquals(expectedDeck, actualDeck);
        Mockito.verify(cardEnrichmentService, Mockito.times(19)).findCardInJson(Mockito.anyInt(), Mockito.anyObject());
    }

    /**
     * Test the map from method.
     */
    @Test
    public void testMapFrom() {

        // Setup deck to run test on.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419, 333);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927, 43502, 123);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass());
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass());
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deckToTest = deckBuilder.build();

        // Setup deck to run test on.
        final DeckControllerModel expectedDeck = new DeckControllerModel();
        expectedDeck.setFormat(format.getFormatName());
        expectedDeck.setStartingHero(hero.getHeroClass());
        expectedDeck.setOneOfCards(oneOfIds);
        expectedDeck.setTwoOfCards(twoOfIds);

        // Perform test
        final DeckControllerModel actualDeck = deckMapper.mapFrom(deckToTest);

        // Assert
        Assert.assertEquals(expectedDeck, actualDeck);

    }
}
