/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller.mapper.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hearthstone.deck.string.controller.mapper.DeckStringControllerModelMapper;
import com.hearthstone.deck.string.controller.mapper.DeckStringControllerModelMapperHandler;
import com.hearthstone.deck.string.controller.model.DeckStringControllerModel;

/**
 * Unit test for {@link DeckStringControllerModelMapper}
 */
public class DeckStringControllerModelMapperTest {

    /** The string being used for this test. */
    private static final String DECK_STRING_FOR_TEST = "Unnerfed combo druid deck";

    /** Class under test. */
    private DeckStringControllerModelMapper deckStringMapper;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        this.deckStringMapper = new DeckStringControllerModelMapperHandler();
    }

    /**
     * Test the map to method.
     */
    @Test
    public void testMapTo() {

        // Setup
        final DeckStringControllerModel expectedDeckStringControllerModel = new DeckStringControllerModel();
        expectedDeckStringControllerModel.setDeckString(DECK_STRING_FOR_TEST);

        // Run test
        final DeckStringControllerModel actualdeckStringControllerModel = deckStringMapper.mapTo(DECK_STRING_FOR_TEST);

        // Assert
        Assert.assertEquals(expectedDeckStringControllerModel, actualdeckStringControllerModel);
    }

    /**
     * Test the map from method.
     */
    @Test
    public void testMapFrom() {

        // Setup
        final DeckStringControllerModel deckModelToTest = new DeckStringControllerModel();
        deckModelToTest.setDeckString(DECK_STRING_FOR_TEST);

        // Run test
        final String actualDeckString = deckStringMapper.mapFrom(deckModelToTest);

        // Assert
        Assert.assertEquals(DECK_STRING_FOR_TEST, actualDeckString);
    }

}
