/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller;

import javax.ws.rs.ApplicationPath;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * The hearthstone deck string application. Extends {@link ResourceConfig}
 */
@ApplicationPath("/hsds")
public class HearthstoneDeckStringApplication extends ResourceConfig {

    /**
     * Instantiates a new Hearthstone deck string application.
     */
    public HearthstoneDeckStringApplication() {

        register(HearthstoneDeckStringApplicationEventListener.class);

        property(MarshallerProperties.JSON_INCLUDE_ROOT, true);
    }

}
