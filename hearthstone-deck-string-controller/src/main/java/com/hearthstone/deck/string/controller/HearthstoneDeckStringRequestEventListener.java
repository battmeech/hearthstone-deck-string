/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.HearthstoneDeckStringRequestContext;

/**
 * Implementation of {@link RequestEventListener} for the Hearthstone deck
 * string application.
 *
 * @see HearthstoneDeckStringRequestEventEvent
 */
@RequestScoped
public class HearthstoneDeckStringRequestEventListener implements RequestEventListener {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(HearthstoneDeckStringRequestEventListener.class);

    /** The request context. */
    private HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext;

    /**
     * On event.
     *
     * @param event
     *            the event
     * @InheritDoc
     */
    @Override
    public void onEvent(final RequestEvent event) {
        // NOT MUCH HAPPENS ON EVENTS YET

        if (event.getType() == RequestEvent.Type.RESOURCE_METHOD_START) {
            hearthstoneDeckStringRequestContext.setUniqueRequestReference();
        }

        if (event.getType() == RequestEvent.Type.ON_EXCEPTION) {
            LOG.debug("Error message sent to consumer.");

        }

    }

    /**
     * Sets the request context.
     *
     * @param requestContext
     *            the new request context
     */
    @Inject
    public void setHearthstoneDeckStringRequestContext(
            final HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext) {
        this.hearthstoneDeckStringRequestContext = hearthstoneDeckStringRequestContext;
    }

}
