/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller.exception.mapper;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.HearthstoneDeckStringLogMessages;
import com.hearthstone.deck.string.config.HearthstoneDeckStringRequestContext;
import com.hearthstone.deck.string.config.exception.CardNotFoundException;
import com.hearthstone.deck.string.config.exception.GenericException;
import com.hearthstone.deck.string.controller.model.ErrorResponseModel;

/**
 * Implementation of ExceptionMapper for {@link CardNotFoundException}
 */
@Provider
public class GenericExceptionMapper implements ExceptionMapper<GenericException> {

    /** Logger. */
    public static Logger LOG = LoggerFactory.getLogger(InvalidDeckExceptionMapper.class);

    /** The hearthstone deck string request context. */
    private HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext;

    /**
     * @InheritDoc
     */
    @Override
    public Response toResponse(final GenericException exception) {

        final ErrorResponseModel errorResponseModel = new ErrorResponseModel();
        errorResponseModel.setDescription(exception.getDescription());
        errorResponseModel.setMessage(exception.getWebApplicationException().getMessage());

        LOG.info(String.format("%s - %s - %s", HearthstoneDeckStringLogMessages.ERROR_TO_CONSUMER,
                hearthstoneDeckStringRequestContext.getUniqueRequestReference(),
                exception.getWebApplicationException().getMessage()));

        return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(errorResponseModel)
                .type(MediaType.APPLICATION_JSON).build();
    }

    /**
     * Sets the hearthstone deck string request context.
     *
     * @param hearthstoneDeckStringRequestContext
     *            the new hearthstone deck string request context
     */
    @Inject
    public void setHearthstoneDeckStringRequestContext(
            final HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext) {
        this.hearthstoneDeckStringRequestContext = hearthstoneDeckStringRequestContext;
    }
}
