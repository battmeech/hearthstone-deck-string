/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller;

import java.io.IOException;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.HearthstoneDeckStringLogMessages;
import com.hearthstone.deck.string.config.HearthstoneDeckStringRequestContext;
import com.hearthstone.deck.string.controller.mapper.DeckMapper;
import com.hearthstone.deck.string.controller.mapper.DeckStringControllerModelMapper;
import com.hearthstone.deck.string.controller.model.DeckControllerModel;
import com.hearthstone.deck.string.controller.model.DeckStringControllerModel;
import com.hearthstone.deck.string.controller.model.ErrorResponseModel;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;
import com.hearthstone.deck.string.domain.request.RandomRequest;
import com.hearthstone.deck.string.domain.request.RandomRequest.RandomRequestBuilder;
import com.hearthstone.deck.string.service.RequestService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The REST request controller for all requests which can go to this service.
 */
@Path("deck-string")
@ManagedBean
public class RequestController {

    /** Logger */
    public static final Logger LOG = LoggerFactory.getLogger(RequestController.class);

    /** The deck mapper. */
    private DeckMapper deckMapper;

    /** The deck string response model mapper. */
    private DeckStringControllerModelMapper deckStringControllerModelMapper;

    /** The request service. */
    private RequestService requestService;

    /** The hearthstone deck string request context. */
    private HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext;

    /**
     * New deck string.
     *
     * @param deckControllerModel
     *            the deck controller model
     * @return the response
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Takes in a Hearthstone deck, and converts it into a deck string.", notes = "Valides decks to ensure that they're valid.", response = DeckStringControllerModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpStatus.SC_OK, message = "OK", response = DeckStringControllerModel.class),
            @ApiResponse(code = HttpStatus.SC_BAD_REQUEST, message = "Bad Request", response = ErrorResponseModel.class),
            @ApiResponse(code = HttpStatus.SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error", response = ErrorResponseModel.class) })
    public Response newDeckString(
            @ApiParam(name = "deckList", value = "The deck to be encoded") @Valid final DeckControllerModel deckControllerModel) {

        LOG.info(String.format("%s - %s", HearthstoneDeckStringLogMessages.DECK_STRING_REQUEST_RECEIVED,
                hearthstoneDeckStringRequestContext.getUniqueRequestReference()));
        final Deck deck = deckMapper.mapTo(deckControllerModel);

        final String deckString = requestService.processDeck(deck);

        final DeckStringControllerModel deckStringResponseModel = deckStringControllerModelMapper.mapTo(deckString);

        LOG.info(String.format("%s - %s", HearthstoneDeckStringLogMessages.DECK_STRING_RESPONSE_SENT,
                hearthstoneDeckStringRequestContext.getUniqueRequestReference()));
        return Response.ok().entity(deckStringResponseModel).build();
    }

    /**
     * New deck request.
     *
     * @param deckStringControllerModel
     *            the deck string controller model
     * @return the response
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @POST
    @Path("new-deck")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Takes in a Hearthstone deck string, and converts it into a deck object.", notes = "Valides decks to ensure that they're valid.", response = DeckStringControllerModel.class)
    @ApiResponses(value = { @ApiResponse(code = HttpStatus.SC_OK, message = "OK", response = DeckControllerModel.class),
            @ApiResponse(code = HttpStatus.SC_BAD_REQUEST, message = "Bad Request", response = ErrorResponseModel.class),
            @ApiResponse(code = HttpStatus.SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error", response = ErrorResponseModel.class) })
    public Response newDeck(
            @ApiParam(name = "deckList", value = "The deck string to be converted") @Valid final DeckStringControllerModel deckStringControllerModel)
            throws IOException {

        LOG.info(String.format("%s - %s", HearthstoneDeckStringLogMessages.DECK_REQUEST_RECEIVED,
                hearthstoneDeckStringRequestContext.getUniqueRequestReference()));
        final String deckString = deckStringControllerModelMapper.mapFrom(deckStringControllerModel);

        final Deck deck = requestService.processDeckString(deckString);

        final DeckControllerModel deckControllerModel = deckMapper.mapFrom(deck);

        LOG.info(String.format("%s - %s", HearthstoneDeckStringLogMessages.DECK_RESPONSE_SENT,
                hearthstoneDeckStringRequestContext.getUniqueRequestReference()));
        return Response.ok().entity(deckControllerModel).build();
    }

    /**
     * New deck request.
     *
     * @param deckStringControllerModel
     *            the deck string controller model
     * @return the response
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @GET
    @Path("random")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Sends the consumer a random deck to use.", response = DeckStringControllerModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpStatus.SC_OK, message = "OK", response = DeckStringControllerModel.class),
            @ApiResponse(code = HttpStatus.SC_BAD_REQUEST, message = "Bad Request", response = ErrorResponseModel.class),
            @ApiResponse(code = HttpStatus.SC_INTERNAL_SERVER_ERROR, message = "Internal Server Error", response = ErrorResponseModel.class) })
    public Response randomDeck(@QueryParam(value = "startingHero") final String startingHero,
            @QueryParam(value = "formatName") final String formatName) throws IOException {

        LOG.info(String.format("%s - %s", HearthstoneDeckStringLogMessages.RANDOM_DECK_STRING_RECEIVED,
                hearthstoneDeckStringRequestContext.getUniqueRequestReference()));

        StartingHero selectedHero = null;
        if (StringUtils.isNotBlank(startingHero) && StartingHero.validateClass(startingHero)) {
            selectedHero = StartingHero.findValueFromClass(startingHero);
        }
        Format selectedFormat = null;
        if (StringUtils.isNotBlank(formatName) && Format.validateFormat(formatName)) {
            selectedFormat = Format.findValueFromName(formatName);
        }

        final RandomRequest randomRequest = new RandomRequestBuilder().withStartingHero(selectedHero)
                .withFormat(selectedFormat).build();

        final String deckString = requestService.randomDeck(randomRequest);

        final DeckStringControllerModel deckStringResponseModel = deckStringControllerModelMapper.mapTo(deckString);

        LOG.info(String.format("%s - %s", HearthstoneDeckStringLogMessages.RANDOM_DECK_RESPONSE_SENT,
                hearthstoneDeckStringRequestContext.getUniqueRequestReference()));
        return Response.ok().entity(deckStringResponseModel).build();
    }

    /**
     * Set the deck mapper
     *
     * @param deckMapper
     *            the deck mapper
     */
    @Inject
    public void setDeckMapper(final DeckMapper deckMapper) {
        this.deckMapper = deckMapper;
    }

    /**
     * Set the request service
     *
     * @param requestService
     *            the request service
     */
    @Inject
    public void setRequestService(final RequestService requestService) {
        this.requestService = requestService;
    }

    /**
     * Set the deck string controller model mapper
     *
     * @param deckStringControllerModelMapper
     *            the deck String Controller Model Mapper
     */
    @Inject
    public void setDeckStringControllerModelMapper(
            final DeckStringControllerModelMapper deckStringControllerModelMapper) {
        this.deckStringControllerModelMapper = deckStringControllerModelMapper;
    }

    /**
     * Sets the hearthstone deck string request context.
     *
     * @param hearthstoneDeckStringRequestContext
     *            the new hearthstone deck string request context
     */
    @Inject
    public void setHearthstoneDeckStringRequestContext(
            final HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext) {
        this.hearthstoneDeckStringRequestContext = hearthstoneDeckStringRequestContext;
    }
}
