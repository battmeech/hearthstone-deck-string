/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Produces;

import org.apache.deltaspike.scheduler.impl.SchedulerProducer;
import org.apache.deltaspike.scheduler.spi.Scheduler;
import org.quartz.Job;

/**
 * Job scheduler. Extends {@link SchedulerProducer}
 */
@ApplicationScoped
public class JobScheduler extends SchedulerProducer {

    /**
     * @InheritDoc
     */
    @Override
    @SuppressWarnings("unchecked")
    @Produces
    @ApplicationScoped
    protected Scheduler<Job> produceScheduler() {
        return super.produceScheduler();
    }
}
