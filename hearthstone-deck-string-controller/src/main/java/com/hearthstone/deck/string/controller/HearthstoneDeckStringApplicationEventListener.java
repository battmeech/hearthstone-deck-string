/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.controller;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.HearthstoneDeckStringLogMessages;

/**
 * Implementation of {@link ApplicationEventListener} for the Hearthstone deck
 * string application
 */
@ApplicationScoped
public class HearthstoneDeckStringApplicationEventListener implements ApplicationEventListener {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(HearthstoneDeckStringApplicationEventListener.class);

    /** The hearthstone deck string request event listener. */
    @Inject
    private HearthstoneDeckStringRequestEventListener hearthstoneDeckStringRequestEventListener;

    /**
     * @InheritDoc
     */
    @Override
    public void onEvent(final ApplicationEvent event) {
        if (event.getType() == ApplicationEvent.Type.INITIALIZATION_FINISHED) {
            LOG.info(HearthstoneDeckStringLogMessages.SERVICE_STARTED);
        } else if (event.getType() == ApplicationEvent.Type.DESTROY_FINISHED) {
            LOG.info(HearthstoneDeckStringLogMessages.SERVICE_STOPPED);
        }
    }

    /**
     * @InheritDoc
     */
    @Override
    public RequestEventListener onRequest(final RequestEvent requestEvent) {
        return hearthstoneDeckStringRequestEventListener;
    }

}
