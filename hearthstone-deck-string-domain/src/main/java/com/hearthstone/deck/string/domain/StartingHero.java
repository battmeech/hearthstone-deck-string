/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain;

import javax.ws.rs.WebApplicationException;

import org.apache.commons.httpclient.HttpStatus;

import com.hearthstone.deck.string.config.ErrorDescriptionConstants;
import com.hearthstone.deck.string.config.exception.GenericException;

/** Enum of starting heroes. */
public enum StartingHero {

    /** Malfurion - druid. */
    MALFURION(274, "Druid"),

    /** Rexxar - hunter. */
    REXXAR(31, "Hunter"),

    /** Jaina - mage. */
    JAINA(637, "Mage"),

    /** Uther - paladin. */
    UTHER(671, "Paladin"),

    /** Anduin - priest. */
    ANDUIN(813, "Priest"),

    /** Valeera - rogue. */
    VALEERA(930, "Rogue"),

    /** Thrall - shaman. */
    THRALL(1066, "Shaman"),

    /** Gul'Dan - warlock. */
    GULDAN(893, "Warlock"),

    /** Garrosh - warrior. */
    GARROSH(7, "Warrior");

    /** Hero ID. */
    private int heroId;

    /** Hero Class. */
    private String heroClass;

    /**
     * Instantiates a new Starting Hero
     *
     * @param heroId
     *            the heroId
     *
     * @param heroClass
     *            the heroClass
     */
    private StartingHero(final int heroId, final String heroClass) {
        this.heroId = heroId;
        this.heroClass = heroClass;
    }

    /**
     * Gets the heroId
     *
     * @return the heroId
     */
    public int getHeroId() {
        return heroId;
    }

    /**
     * Gets the heroClass
     *
     * @return the heroClass
     */
    public String getHeroClass() {
        return heroClass;
    }

    /**
     * Validate that a class being passed in is actually a valid one. This method
     * will be used in future when lists of decks will be able to be passed in as a
     * POST request, and the class header at the top needs to be validated.
     */
    public static boolean validateClass(final String classCheck) {
        for (final StartingHero startingHero : StartingHero.values()) {
            if (classCheck.equalsIgnoreCase(startingHero.getHeroClass())) {
                return true;
            }
        }
        throw new GenericException(new WebApplicationException("Invalid hero supplied.", HttpStatus.SC_BAD_REQUEST),
                String.format(ErrorDescriptionConstants.INVALID_STARTING_HERO, classCheck));
    }

    /**
     * Check if a dbfId is a starting hero.
     *
     * @param classCheck
     *            the class check
     * @return true, if successful
     */
    public static boolean isHeroClass(final int dbfId) {
        for (final StartingHero startingHero : StartingHero.values()) {
            if (dbfId == startingHero.getHeroId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Find value from ID.
     *
     * @param id
     *            the id
     * @return the starting hero
     */
    public static StartingHero findValueFromId(final int id) {
        for (final StartingHero startingHero : StartingHero.values()) {
            if (id == startingHero.getHeroId()) {
                return startingHero;
            }
        }
        throw new IllegalStateException("Invalid hero ID supplied.");
    }

    /**
     * Find value from class.
     *
     * @param heroClass
     *            the hero class
     * @return the starting hero
     */
    public static StartingHero findValueFromClass(final String heroClass) {
        for (final StartingHero startingHero : StartingHero.values()) {
            if (heroClass.equalsIgnoreCase(startingHero.getHeroClass())) {
                return startingHero;
            }
        }
        throw new IllegalStateException("Invalid hero ID supplied.");
    }
}
