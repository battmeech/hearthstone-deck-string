/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain;
/*
 * Hearthstone Deck String Converter - Matt Beech
 */

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * The validation result.
 */
public class ValidationResult {

    /** The message. */
    private List<String> message;

    /** The validation status. */
    private ValidationStatus validationStatus;

    /**
     * Gets the message.
     *
     * @return the message
     */
    public List<String> getMessage() {
        return message;
    }

    /**
     * Sets the message.
     *
     * @param message
     *            the new message
     */
    public void setMessage(final List<String> message) {
        this.message = message;
    }

    /**
     * Gets the validation status.
     *
     * @return the validation status
     */
    public ValidationStatus getValidationStatus() {
        return validationStatus;
    }

    /**
     * Sets the validation status.
     *
     * @param validationStatus
     *            the new validation status
     */
    public void setValidationStatus(final ValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }

    /**
     * @InheritDoc
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equal = false;

        if (obj instanceof ValidationResult) {
            final ValidationResult otherObj = (ValidationResult) obj;

            final EqualsBuilder equalsBuilder = new EqualsBuilder();

            equalsBuilder.append(message, otherObj.message).append(validationStatus, otherObj.validationStatus);

            equal = equalsBuilder.isEquals();
        }

        return equal;
    }

    /**
     * @InheritDoc
     */
    @Override
    public int hashCode() {
        final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();

        hashCodeBuilder.append(message).append(validationStatus);

        final int hashCode = hashCodeBuilder.hashCode();

        return hashCode;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append(message).append(validationStatus);

        return toStringBuilder.toString();
    }

}
