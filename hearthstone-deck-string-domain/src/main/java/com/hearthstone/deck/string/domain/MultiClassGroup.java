/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain;

import java.util.Arrays;
import java.util.List;

/**
 * This is an enum of tri classes to handle the three tri classes which are in
 * the game.
 */
public enum MultiClassGroup {

    /** Grimy Goons. */
    GOONS("GRIMY_GOONS", Arrays.asList("WARRIOR", "HUNTER", "PALADIN")),

    /** The Kabal. */
    KABAL("KABAL", Arrays.asList("MAGE", "WARLOCK", "PRIEST")),

    /** Jade Lotus. */
    JADE("JADE_LOTUS", Arrays.asList("DRUID", "ROGUE", "SHAMAN"));

    /** The class group. */
    private String classGroup;

    /** The classes in group. */
    private List<String> classesInGroup;

    /**
     * Instantiates a new multi class group.
     *
     * @param classGroup
     *            the class group
     * @param classesInGroup
     *            the classes in group
     */
    private MultiClassGroup(final String classGroup, final List<String> classesInGroup) {
        this.classGroup = classGroup;
        this.classesInGroup = classesInGroup;
    }

    /**
     * Gets the class group.
     *
     * @return the class group
     */
    public String getClassGroup() {
        return classGroup;
    }

    /**
     * Gets the classes in group.
     *
     * @return the classes in group
     */
    public List<String> getClassesInGroup() {
        return classesInGroup;
    }

    /**
     * Find the class group from the one given in JSON.
     *
     * @param classGroupToFind
     *            the class group to find
     * @return the multi class group
     */
    public static MultiClassGroup findFromClassGroup(final String classGroupToFind) {
        for (final MultiClassGroup multiClassGroup : MultiClassGroup.values()) {
            if (classGroupToFind.equalsIgnoreCase(multiClassGroup.getClassGroup())) {
                return multiClassGroup;
            }
        }
        throw new IllegalStateException("Invalid class group given");
    }
}
