/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain;

import javax.ws.rs.WebApplicationException;

import org.apache.commons.httpclient.HttpStatus;

import com.hearthstone.deck.string.config.ErrorDescriptionConstants;
import com.hearthstone.deck.string.config.exception.GenericException;

/** Enum of Hearthstone formats. */
public enum Format {

    /** Unknown format. */
    FT_UNKNOWN(0, "Unknown"),

    /** Wild format. */
    FT_WILD(1, "Wild"),

    /** Standard format. */
    FT_STANDARD(2, "Standard");

    /** The format integer */
    private int formatId;

    /** The format name. */
    private String formatName;

    /**
     * Instantiates a new format.
     *
     * @param format
     *            the format
     * @param formatName
     *            the format name
     */
    private Format(final int format, final String formatName) {
        this.formatId = format;
        this.formatName = formatName;
    }

    /**
     * Gets the format
     *
     * @return the format
     */
    public int getFormatId() {
        return formatId;
    }

    /**
     * Gets the format name.
     *
     * @return the format name
     */
    public String getFormatName() {
        return formatName;
    }

    /**
     * Validate that a format being passed in is actually a valid one. This method
     * will be used in future when lists of decks will be able to be passed in as a
     * POST request, and the class header at the top needs to be validated.
     */
    public static boolean validateFormat(final String formatCheck) {
        for (final Format format : Format.values()) {
            if (formatCheck.equalsIgnoreCase(format.getFormatName())) {
                return true;
            }
        }
        throw new GenericException(new WebApplicationException("Invalid format supplied.", HttpStatus.SC_BAD_REQUEST),
                String.format(ErrorDescriptionConstants.INVALID_CARD_FORMAT, formatCheck));
    }

    /**
     * Find value from id.
     *
     * @param id
     *            the id
     * @return the format
     */
    public static Format findValueFromId(final int id) {
        for (final Format format : Format.values()) {
            if (id == format.getFormatId()) {
                return format;
            }
        }

        // If cannot find the format, just return unknown format.
        return Format.FT_UNKNOWN;
    }

    /**
     * Find value from name.
     *
     * @param formatName
     *            the format name
     * @return the format
     */
    public static Format findValueFromName(final String formatName) {
        for (final Format format : Format.values()) {
            if (formatName.equalsIgnoreCase(format.getFormatName())) {
                return format;
            }
        }

        // If cannot find the format, just return unknown format.
        return Format.FT_UNKNOWN;
    }
}
