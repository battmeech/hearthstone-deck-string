/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain.request;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;

/**
 * The random request model - used when a consumer asks for a random deck,
 * allowing some parameters to be set.
 */
public class RandomRequest {

    /** The starting hero of the random deck. */
    private final StartingHero startingHero;

    /** The format of the random deck. */
    private final Format format;

    /**
     * Instantiates a new random request.
     *
     * @param builder
     *            the builder
     */
    private RandomRequest(final RandomRequestBuilder builder) {
        this.startingHero = builder.startingHero;
        this.format = builder.format;
    }

    /**
     * Gets the starting hero.
     *
     * @return the starting hero
     */
    public StartingHero getStartingHero() {
        return startingHero;
    }

    /**
     * Gets the format.
     *
     * @return the format
     */
    public Format getFormat() {
        return format;
    }

    /**
     * Builder for the Random Request model.
     */
    public static class RandomRequestBuilder {

        /** The starting hero of the random deck. */
        private StartingHero startingHero;

        /** The format of the random deck. */
        private Format format;

        /**
         * Instantiates a new random request builder.
         */
        public RandomRequestBuilder() {

        }

        /**
         * With starting hero.
         *
         * @param startingHero
         *            the starting hero
         * @return the random request builder
         */
        public RandomRequestBuilder withStartingHero(final StartingHero startingHero) {
            this.startingHero = startingHero;
            return this;
        }

        /**
         * With format.
         *
         * @param format
         *            the format
         * @return the random request builder
         */
        public RandomRequestBuilder withFormat(final Format format) {
            this.format = format;
            return this;
        }

        /**
         * Builds the random request.
         *
         * @return the random request
         */
        public final RandomRequest build() {
            return new RandomRequest(this);
        }

    }

    /**
     * @InheritDoc
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equal = false;

        if (obj instanceof RandomRequest) {
            final RandomRequest otherObj = (RandomRequest) obj;

            final EqualsBuilder equalsBuilder = new EqualsBuilder();

            equalsBuilder.append(startingHero, otherObj.startingHero).append(format, otherObj.format);

            equal = equalsBuilder.isEquals();
        }

        return equal;
    }

    /**
     * @InheritDoc
     */
    @Override
    public int hashCode() {
        final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();

        hashCodeBuilder.append(startingHero).append(format);

        final int hashCode = hashCodeBuilder.hashCode();

        return hashCode;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append(startingHero).append(format);

        return toStringBuilder.toString();
    }

}
