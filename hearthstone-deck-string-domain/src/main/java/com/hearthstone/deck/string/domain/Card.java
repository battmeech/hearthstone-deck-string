/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Card model - this is used when reading the JSON file to enrich card
 * information.
 */
@JsonDeserialize(builder = Card.CardBuilder.class)
public class Card {

    /**
     * Unique ID used to identify each card.
     */
    private final int dbfId;

    /**
     * The cards class, used to determine if a card is allowed in a certain deck.
     */
    private final String cardClass;

    /**
     * The cards name.
     */
    private final String name;

    /**
     * The rarity of the card - needed for validation when checking if more than one
     * legendary is used.
     */
    private final String rarity;

    /** The multi class group - needed for validation when checking card class. */
    private final String multiClassGroup;

    /** The expansion - needed for validation when checking deck format. */
    private final String set;

    /** The mana cost of the card. */
    private final int cost;

    /**
     * Instantiates a new card.
     *
     * @param builder
     *            the builder
     */
    private Card(final CardBuilder builder) {
        this.dbfId = builder.dbfId;
        this.cardClass = builder.cardClass;
        this.name = builder.name;
        this.rarity = builder.rarity;
        this.multiClassGroup = builder.multiClassGroup;
        this.set = builder.set;
        this.cost = builder.cost;
    }

    /**
     * Gets the dbf id.
     *
     * @return the dbf id
     */
    public int getDbfId() {
        return dbfId;
    }

    /**
     * Gets the card class.
     *
     * @return the card class
     */
    public String getCardClass() {
        return cardClass;
    }

    /**
     * Gets the card name.
     *
     * @return the card name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the rarity.
     *
     * @return the rarity
     */
    public String getRarity() {
        return rarity;
    }

    /**
     * Gets the multi class group.
     *
     * @return the multi class group
     */
    public String getMultiClassGroup() {
        return multiClassGroup;
    }

    /**
     * Gets the sets the.
     *
     * @return the sets the
     */
    public String getSet() {
        return set;
    }

    /**
     * Gets the cost.
     *
     * @return the cost
     */
    public int getCost() {
        return cost;
    }

    /**
     * Builder for the Deck model.
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CardBuilder {

        /** The dbf id. */
        private int dbfId;

        /** The card class. */
        private String cardClass;

        /** The name. */
        private String name;

        /**
         * The rarity of the card - needed for validation when checking if more than one
         * legendary is used.
         */
        private String rarity;

        /** The multi class group - needed for validation when checking card class. */
        private String multiClassGroup;

        /** The expansion - needed for validation when checking deck format. */
        private String set;

        /** The mana cost of the card. */
        private int cost;

        /**
         * Instantiates a new card builder.
         */
        public CardBuilder() {

        }

        /**
         * With dbf id.
         *
         * @param dbfId
         *            the dbf id
         * @return the card builder
         */
        @JsonProperty
        public CardBuilder withDbfId(final int dbfId) {
            this.dbfId = dbfId;
            return this;
        }

        /**
         * With card class.
         *
         * @param cardClass
         *            the card class
         * @return the card builder
         */
        @JsonProperty
        public CardBuilder withCardClass(final String cardClass) {
            this.cardClass = cardClass;
            return this;
        }

        /**
         * With one of cards.
         *
         * @param name
         *            the name
         * @return the card builder
         */
        @JsonProperty
        public CardBuilder withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * With rarity.
         *
         * @param rarity
         *            the rarity
         * @return the card builder
         */
        @JsonProperty
        public CardBuilder withRarity(final String rarity) {
            this.rarity = rarity;
            return this;
        }

        /**
         * With multi class group.
         *
         * @param multiClassGroup
         *            the multi class group
         * @return the card builder
         */
        @JsonProperty
        public CardBuilder withMultiClassGroup(final String multiClassGroup) {
            this.multiClassGroup = multiClassGroup;
            return this;
        }

        /**
         * With set.
         *
         * @param set
         *            the set
         * @return the card builder
         */
        @JsonProperty
        public CardBuilder withSet(final String set) {
            this.set = set;
            return this;
        }

        /**
         * With cost.
         *
         * @param cost
         *            the cost
         * @return the card builder
         */
        @JsonProperty
        public CardBuilder withCost(final int cost) {
            this.cost = cost;
            return this;
        }

        /**
         * Builds the.
         *
         * @return the deck
         */
        public final Card build() {
            return new Card(this);
        }
    }

    /**
     * Equals.
     *
     * @param obj
     *            the obj
     * @return true, if successful
     * @InheritDoc
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equal = false;

        if (obj instanceof Card) {
            final Card otherObj = (Card) obj;

            final EqualsBuilder equalsBuilder = new EqualsBuilder();

            equalsBuilder.append(dbfId, otherObj.dbfId).append(name, otherObj.name)
                    .append(cardClass, otherObj.cardClass).append(rarity, otherObj.rarity)
                    .append(multiClassGroup, otherObj.multiClassGroup).append(set, otherObj.set)
                    .append(cost, otherObj.cost);

            equal = equalsBuilder.isEquals();
        }

        return equal;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @InheritDoc
     */
    @Override
    public int hashCode() {
        final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();

        hashCodeBuilder.append(dbfId).append(name).append(cardClass).append(rarity).append(multiClassGroup).append(set)
                .append(cost);

        final int hashCode = hashCodeBuilder.hashCode();

        return hashCode;
    }

    /**
     * To string.
     *
     * @return the string
     * @InheritDoc
     */
    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append(dbfId)
                .append(name).append(cardClass).append(rarity).append(multiClassGroup).append(set).append(cost);

        return toStringBuilder.toString();
    }

}
