/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain;

/**
 * Various states of validation.
 */
public enum ValidationStatus {

    /**
     * Deck was too short, this is still a valid deck but should probably come with
     * a warning.
     */
    INCOMPLETE,

    /** Invalid class cards or deck being over 30 cards has made it invalid. */
    INVALID,

    /** Successful 30 card deck. */
    VALID;

}
