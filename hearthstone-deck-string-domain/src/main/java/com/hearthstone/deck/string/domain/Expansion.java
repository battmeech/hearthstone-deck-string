/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain;

import java.util.Arrays;
import java.util.List;

/**
 * Enum of expansions.
 */
public enum Expansion {

    /** Basic set. */
    BASIC("CORE", Arrays.asList(Format.FT_STANDARD, Format.FT_WILD)),

    /** Classic set. */
    CLASSIC("EXPERT1", Arrays.asList(Format.FT_STANDARD, Format.FT_WILD)),

    /** The hall of fame. */
    HALL_OF_FAME("HOF", Arrays.asList(Format.FT_WILD)),

    /** Naxxramas. */
    NAXXRAMAS("NAXX", Arrays.asList(Format.FT_WILD)),

    /** Goblins VS Gnomes. */
    GOBLINS_VS_GNOMES("GVG", Arrays.asList(Format.FT_WILD)),

    /** Black Rock Mountain. */
    BLACK_ROCK_MOUNTAIN("BRM", Arrays.asList(Format.FT_WILD)),

    /** The Grand Tournament. */
    GRAND_TOURNAMENT("TGT", Arrays.asList(Format.FT_WILD)),

    /** The League of Explorers. */
    LEAGUE_OF_EXPLORERS("LOE", Arrays.asList(Format.FT_WILD)),

    /** Whispers of the Old Gods. */
    OLD_GODS("OG", Arrays.asList(Format.FT_WILD)),

    /** One Night in Karazhan */
    KARAZHAN("KARA", Arrays.asList(Format.FT_WILD)),

    /** Mean Streets of Gadgetzan. */
    GADGETZAN("GANGS", Arrays.asList(Format.FT_WILD)),

    /** Journey to Un'Goro. */
    UNGORO("UNGORO", Arrays.asList(Format.FT_STANDARD, Format.FT_WILD)),

    /** Knights of the Frozen Throne. */
    FROZEN_THRONE("ICECROWN", Arrays.asList(Format.FT_STANDARD, Format.FT_WILD)),

    /** Kobolds & Catacombs. */
    KOBOLDS("LOOTAPALOOZA", Arrays.asList(Format.FT_STANDARD, Format.FT_WILD)),

    /** The Witchwood. */
    WITCHWOOD("GILNEAS", Arrays.asList(Format.FT_STANDARD, Format.FT_WILD));

    /** The expansions name as it appears on the JSON file. */
    private String expansionCode;

    /** The expansions format. */
    private List<Format> expansionFormat;

    /**
     * Instantiates a new expansion.
     *
     * @param expansionCode
     *            the expansion code
     * @param expansionFormat
     *            the expansion format
     */
    private Expansion(final String expansionCode, final List<Format> expansionFormat) {
        this.expansionCode = expansionCode;
        this.expansionFormat = expansionFormat;
    }

    /**
     * Gets the expansion code.
     *
     * @return the expansion code
     */
    public String getExpansionCode() {
        return expansionCode;
    }

    /**
     * Gets the expansion format.
     *
     * @return the expansion format
     */
    public List<Format> getExpansionFormat() {
        return expansionFormat;
    }

    /**
     * Gets the expansion from the expansion code provided.
     *
     * @param code
     *            the code
     * @return the expansion from code
     */
    public static Expansion getExpansionFromCode(final String code) {
        for (final Expansion expansion : Expansion.values()) {
            if (expansion.getExpansionCode().equalsIgnoreCase(code)) {
                return expansion;
            }
        }
        return null;
    }

}
