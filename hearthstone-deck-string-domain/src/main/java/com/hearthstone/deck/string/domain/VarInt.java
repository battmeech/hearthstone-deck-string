/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;

import org.apache.commons.httpclient.HttpStatus;

import com.hearthstone.deck.string.config.exception.GenericException;

/**
 * The VarInt object, the deck strings are made up of multiple VarInts, each
 * representing something different.
 */
public class VarInt {

    /**
     * Take in an integer, and write it to a VarInt value.
     *
     * @param dataOutputStream
     * @param value
     * @throws IOException
     */
    public static void writeVarInt(final DataOutputStream dataOutputStream, int value) throws IOException {
        do {
            byte temp = (byte) (value & 0b01111111);
            // >>> means that the sign bit is shifted with the rest of the number rather
            // than being left alone
            value >>>= 7;
            if (value != 0) {
                temp |= 0b10000000;
            }
            dataOutputStream.writeByte(temp);
        } while (value != 0);
    }

    /**
     * Convert a VarInt into a list of Integers.
     *
     * @param stream
     * @return List<Integer>
     * @throws IOException
     */
    public static List<Integer> readVarInt(final DataInputStream stream) throws IOException {
        int numRead = 0;
        int result = 0;
        byte read;
        final List<Integer> data = new ArrayList<>();
        while (stream.available() > 0) {
            do {
                read = stream.readByte();
                final int value = (read & 0b01111111);
                result |= (value << (7 * numRead));
                numRead++;
                if (numRead > 5) {
                    throw new GenericException(
                            new WebApplicationException("Internal Server Error", HttpStatus.SC_INTERNAL_SERVER_ERROR),
                            "VarInt is too big");
                }
            } while ((read & 0b10000000) != 0);

            data.add(result);
            result = 0;
            numRead = 0;
        }
        return data;
    }
}
