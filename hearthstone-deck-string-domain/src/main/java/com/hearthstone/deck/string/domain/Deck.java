/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.domain;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * The deck object.
 */
public class Deck {

    /** The starting hero. */
    private final StartingHero startingHero;

    /** The format. */
    private final Format format;

    /** The one of cards. */
    private final List<Card> oneOfCards;

    /** The two of cards. */
    private final List<Card> twoOfCards;

    /**
     * Instantiates a new deck.
     *
     * @param builder
     *            the builder
     */
    private Deck(final DeckBuilder builder) {
        this.startingHero = builder.startingHero;
        this.format = builder.format;
        this.oneOfCards = builder.oneOfCards;
        this.twoOfCards = builder.twoOfCards;
    }

    /**
     * Gets the starting hero.
     *
     * @return the starting hero
     */
    public StartingHero getStartingHero() {
        return startingHero;
    }

    /**
     * Gets the format.
     *
     * @return the format
     */
    public Format getFormat() {
        return format;
    }

    /**
     * Gets the one of cards.
     *
     * @return the one of cards
     */
    public List<Card> getOneOfCards() {
        return oneOfCards;
    }

    /**
     * Gets the two of cards.
     *
     * @return the two of cards
     */
    public List<Card> getTwoOfCards() {
        return twoOfCards;
    }

    /**
     * Builder for the Deck model.
     */
    public static class DeckBuilder {

        /** The starting hero. */
        private StartingHero startingHero;

        /** The format. */
        private Format format;

        /** The one of cards. */
        private List<Card> oneOfCards;

        /** The two of cards. */
        private List<Card> twoOfCards;

        /**
         * Instantiates a new deck builder.
         */
        public DeckBuilder() {

        }

        /**
         * With starting hero.
         *
         * @param startingHero
         *            the starting hero
         * @return the deck builder
         */
        public DeckBuilder withStartingHero(final StartingHero startingHero) {
            this.startingHero = startingHero;
            return this;
        }

        /**
         * With format.
         *
         * @param format
         *            the format
         * @return the deck builder
         */
        public DeckBuilder withFormat(final Format format) {
            this.format = format;
            return this;
        }

        /**
         * With one of cards.
         *
         * @param oneOfCards
         *            the one of cards
         * @return the deck builder
         */
        public DeckBuilder withOneOfCards(final List<Card> oneOfCards) {
            this.oneOfCards = oneOfCards;
            return this;
        }

        /**
         * With two of cards.
         *
         * @param twoOfCards
         *            the two of cards
         * @return the deck builder
         */
        public DeckBuilder withTwoOfCards(final List<Card> twoOfCards) {
            this.twoOfCards = twoOfCards;
            return this;
        }

        /**
         * Builds the.
         *
         * @return the deck
         */
        public final Deck build() {
            return new Deck(this);
        }
    }

    /**
     * @InheritDoc
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equal = false;

        if (obj instanceof Deck) {
            final Deck otherObj = (Deck) obj;

            final EqualsBuilder equalsBuilder = new EqualsBuilder();

            equalsBuilder.append(startingHero, otherObj.startingHero).append(format, otherObj.format)
                    .append(oneOfCards, otherObj.oneOfCards).append(twoOfCards, otherObj.twoOfCards);

            equal = equalsBuilder.isEquals();
        }

        return equal;
    }

    /**
     * @InheritDoc
     */
    @Override
    public int hashCode() {
        final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();

        hashCodeBuilder.append(startingHero).append(format).append(oneOfCards).append(twoOfCards);

        final int hashCode = hashCodeBuilder.hashCode();

        return hashCode;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append(String.format("Class: %s", startingHero.getHeroClass()))
                .append(String.format("Format: %s", format.getFormatName()));

        for (final Card card : oneOfCards) {
            toStringBuilder.append(card.getName());
        }

        for (final Card card : twoOfCards) {
            toStringBuilder.append(String.format("%s x2", card.getName()));
        }

        return toStringBuilder.toString();
    }

}
