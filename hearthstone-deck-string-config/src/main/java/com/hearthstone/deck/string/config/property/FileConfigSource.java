/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config.property;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.deltaspike.core.spi.config.ConfigSource;

/**
 * An implementation of {@link ConfigSource} which allows the use of an external
 * file.
 */
public class FileConfigSource implements ConfigSource {

    /** The ordinal. */
    private final int ordinal = 150;

    /** The property key. */
    private final String propertyKey = "$%s{%s}";

    /** The properties. */
    private final Properties properties;

    /** The property name. */
    private final String propertyName;

    /** The prefix. */
    private final String prefix;

    /**
     * @InheritDoc
     */
    @Override
    public int getOrdinal() {
        return ordinal;
    }

    /**
     * @InheritDoc
     */
    @Override
    public Map<String, String> getProperties() {
        final Map<String, String> propertyMap = new HashMap<String, String>();
        for (final String propertyName : properties.stringPropertyNames()) {
            propertyMap.put(propertyName, properties.getProperty(propertyName));
        }

        return propertyMap;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String getPropertyValue(final String key) {
        return properties.getProperty(key);
    }

    /**
     * @InheritDoc
     */
    @Override
    public String getConfigName() {
        return propertyName;
    }

    /**
     * @InheritDoc
     */
    @Override
    public boolean isScannable() {
        return true;
    }

    /**
     * Gets the prefix.
     *
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Instantiates a new file config source.
     *
     * @param prefix
     *            the prefix
     * @param propertyFileUrl
     *            the property file url
     * @param file
     *            the file
     */
    public FileConfigSource(final String prefix, final URL propertyFileUrl, final File file) throws IOException {

        final Properties properties = new Properties();

        final Properties internalProperties = new Properties();
        internalProperties.load(propertyFileUrl.openStream());
        final Enumeration<Object> internalKeys = internalProperties.keys();

        while (internalKeys.hasMoreElements()) {
            final String key = (String) internalKeys.nextElement();
            properties.put(String.format(propertyKey, prefix, key), internalProperties.get(key));
        }

        if (null != file) {
            final Properties externalProperties = new Properties();
            externalProperties.load(new FileInputStream(file));
            final Enumeration<Object> externalKeys = internalProperties.keys();

            while (externalKeys.hasMoreElements()) {
                final String key = (String) externalKeys.nextElement();
                properties.put(String.format(propertyKey, prefix, key), externalProperties.get(key));
            }
        }

        this.properties = properties;
        propertyName = propertyFileUrl.toExternalForm();
        this.prefix = prefix;
    }

    /**
     * Instantiates a new file config source.
     *
     * @param prefix
     *            the prefix
     * @param propertyFileUrl
     *            the property file url
     */
    public FileConfigSource(final String prefix, final URL propertyFileUrl) throws IOException {
        this(prefix, propertyFileUrl, null);
    }

}
