/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config;

import java.io.File;
import java.net.URISyntaxException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.config.ConfigProperty;

/**
 * Config class for the hearthstone deck string application
 */
@ApplicationScoped
public class HearthstoneDeckStringConfig {

    /** Whether the JSON is from the API or Flat file. */
    @Inject
    @ConfigProperty(name = "$hearthstone-deck-string{hearthstone.json.method}")
    private String jsonMethod;

    /** The URL where the hearthstone JSON can be found. */
    @Inject
    @ConfigProperty(name = "$hearthstone-deck-string{hearthstone.card.json.url}")
    private String jsonUrl;

    /** The JSON file location. */
    @Inject
    @ConfigProperty(name = "$hearthstone-deck-string{hearthstone.card.json.file.location}")
    private String jsonFileLocation;

    /** The JSON file location. */
    @Inject
    @ConfigProperty(name = "$hearthstone-deck-string{deck.validation}")
    private String deckValidation;

    /**
     * Gets the json method.
     *
     * @return the json method
     */
    public HearthstoneJsonMethod getJsonMethod() {

        HearthstoneJsonMethod hearthstoneJsonMethod;
        try {
            hearthstoneJsonMethod = HearthstoneJsonMethod.valueOf(jsonMethod);
        } catch (final Exception e) {
            // If it can't be read, default to the file. Don't need to throw an exception
            // here.
            hearthstoneJsonMethod = HearthstoneJsonMethod.FILE;
        }

        return hearthstoneJsonMethod;
    }

    /**
     * Gets the json url.
     *
     * @return the json url
     */
    public String getJsonUrl() {
        return jsonUrl;
    }

    /**
     * Gets the json file location.
     *
     * @return the json file location
     */
    public String getJsonFileLocation() {
        if (StringUtils.startsWith(jsonFileLocation, "classpath:")) {
            try {
                jsonFileLocation = new File(getClass().getClassLoader()
                        .getResource(jsonFileLocation.replace("classpath:", StringUtils.EMPTY)).toURI())
                                .getAbsolutePath();
            } catch (final URISyntaxException e) {
                // If it's not found return a blank string.
                jsonFileLocation = StringUtils.EMPTY;
            }
        }
        return jsonFileLocation;
    }

    /**
     * To validate.
     *
     * @return true, if successful
     */
    public boolean toValidate() {
        if (deckValidation.equalsIgnoreCase("false")) {
            return false;
        } else {
            return true;
        }
    }

}
