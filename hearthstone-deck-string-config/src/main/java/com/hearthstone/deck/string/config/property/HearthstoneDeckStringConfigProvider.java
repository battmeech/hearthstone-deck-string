/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config.property;

/**
 * Provides the hearthstone deck string configuration file.
 */
public class HearthstoneDeckStringConfigProvider extends FileConfigSourceProvider {

    /** Prefix for the hearthstone deck string configuration file. */
    private static final String PREFIX = "hearthstone-deck-string";

    /**
     * The command line argument used to find the hearthstone deck string
     * properties.
     */
    private static final String COMMAND_LINE_ARGUMENT = "hearthstone.deck.string.properties.folder";

    /**
     * @InheritDoc
     */
    @Override
    public String getPrefix() {
        return PREFIX;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String getFileLocation() {
        return COMMAND_LINE_ARGUMENT;
    }

}
