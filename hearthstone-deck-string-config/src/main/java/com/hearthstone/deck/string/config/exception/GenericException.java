/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config.exception;

import javax.ws.rs.WebApplicationException;

/**
 * Generic Exception
 */
public class GenericException extends RuntimeException {

    /**
     * Generated UID
     */
    private static final long serialVersionUID = -8006135634071558873L;

    /** The web application exception. */
    private final WebApplicationException webApplicationException;

    /** The description. */
    private final String description;

    /**
     * Gets the web application exception.
     *
     * @return the web application exception
     */
    public WebApplicationException getWebApplicationException() {
        return webApplicationException;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Instantiates a new card not found exception.
     *
     * @param webApplicationException
     *            the web application exception
     * @param description
     *            the description
     */
    public GenericException(final WebApplicationException webApplicationException, final String description) {
        this.webApplicationException = webApplicationException;
        this.description = description;
    }

}
