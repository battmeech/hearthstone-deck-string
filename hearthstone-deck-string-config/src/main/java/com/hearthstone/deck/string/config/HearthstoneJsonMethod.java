/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config;

/**
 * An enum to represent the two locations which the hearthstone JSON can be
 * acquired from.
 */
public enum HearthstoneJsonMethod {

    /** Acquired from the flat file. */
    FILE,

    /** Acquired from the API. */
    URL;

}
