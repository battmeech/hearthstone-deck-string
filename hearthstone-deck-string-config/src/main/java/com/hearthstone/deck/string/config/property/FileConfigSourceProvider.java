/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config.property;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.deltaspike.core.spi.config.ConfigSource;
import org.apache.deltaspike.core.spi.config.ConfigSourceProvider;
import org.apache.deltaspike.core.util.PropertyFileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link ConfigSourceProvider}
 */
public abstract class FileConfigSourceProvider implements ConfigSourceProvider {

    /** Logger. */
    public static final Logger LOG = LoggerFactory.getLogger(FileConfigSourceProvider.class);

    /** Suffix for a properties file. */
    public static final String SUFFIX = ".properties";

    /** Get the prefix for a properties file. */
    public abstract String getPrefix();

    /** Get the file location of the properties file. */
    public abstract String getFileLocation();

    /**
     * @InheritDoc
     */
    @Override
    public List<ConfigSource> getConfigSources() {

        final List<ConfigSource> configSources = new ArrayList<ConfigSource>();
        final String propertyFileName = getPrefix() + SUFFIX;

        final String propertyDir = System.getProperty(getFileLocation());
        final File fileDir = new File(propertyDir);

        File propertiesFile = null;

        if (fileDir.exists()) {
            for (final File file : fileDir.listFiles()) {
                if (file.getName().equalsIgnoreCase(propertyFileName)) {
                    propertiesFile = file;
                }
            }
        }

        try {
            final Enumeration<URL> propertyFileUrls = PropertyFileUtils.resolvePropertyFiles(propertyFileName);

            while (propertyFileUrls.hasMoreElements()) {

                final URL propertyFileUrl = propertyFileUrls.nextElement();
                LOG.debug("Custom config found by DeltaSpike. Name: {}, URL: {}", propertyFileName, propertyFileUrl);
                FileConfigSource configSource = null;

                if (propertiesFile != null) {
                    configSource = new FileConfigSource(getPrefix(), propertyFileUrl, propertiesFile);
                } else {
                    configSource = new FileConfigSource(getPrefix(), propertyFileUrl);
                }

                configSources.add(configSource);

            }
        } catch (final IOException e) {
            throw new IllegalStateException("problem while loading DeltaSpike property files", e);
        }

        return configSources;
    }

}
