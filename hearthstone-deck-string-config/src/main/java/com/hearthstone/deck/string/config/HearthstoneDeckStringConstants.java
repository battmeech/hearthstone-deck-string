/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config;

/**
 * Constants across the Hearthstone deck string project.
 */
public class HearthstoneDeckStringConstants {

    /** Neutral class name. */
    public static final String NEUTRAL_CARD = "NEUTRAL";

    /** Legendary card. */
    public static final String LEGENDARY_CARD = "LEGENDARY";

    /**
     * The user agent header name used for sending a request to the hearthstone json
     * api.
     */
    public static final String USER_AGENT_HEADER_KEY = "User-Agent";

    /**
     * The user agent header value used for sending a request to the hearthstone
     * json api.
     */
    public static final String USER_AGENT_HEADER_VALUE = "Mozilla/5.0 Firefox/26.0";

    /**
     * Instantiates a new Hearthstone deck string constants.
     */
    private HearthstoneDeckStringConstants() {

    }
}
