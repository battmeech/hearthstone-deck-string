/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config;

/**
 * All the messages used in errors throughout the service.
 */
public class ErrorDescriptionConstants {

    /** Invalid class card message */
    public static final String INVALID_CLASS_CARD = " %s can't go in a %s deck.";

    /** Too few cards message. */
    public static final String TOO_FEW_CARDS = "Deck only contained %s cards.";

    /** Duplicate legendary. */
    public static final String DUPLICATE_LEGENDARY = " Contained two copies of %s, which is a legendary card.";

    /** Wild card in standard deck. */
    public static final String INVALID_CARD_FORMAT = " %s can't be played in the selected format.";

    /** Too many cards message. */
    public static final String TOO_MANY_CARDS = " Deck contained %d cards, which is over the card limit!";

    /** Card not found message. */
    public static final String CARD_NOT_FOUND = "String with ID %s could not be found in card list.";

    /** Exception when reading JSON. */
    public static final String JSON_ERROR = "An error occured while assessing Hearthstone cards list.";

    /** Exception occurred when sending a request to the JSON API. */
    public static final String API_ERROR = "Error when sending request to JSON API.";

    /** A status code other than 200 was returned by the JSON API. */
    public static final String INVALID_STATUS_CODE = "Status code %s returned from Hearthstone JSON API";

    /** An invalid starting hero was given in a request. */
    public static final String INVALID_STARTING_HERO = "%s not a valid class.";

    /** An invalid format was given in a request. */
    public static final String INVALID_FORMAT = "%s not a valid format.";

    /** Cannot have a hero in a deck. */
    public static final String HERO_IN_DECK = "%s is a hero, and cannot be in a deck.";

    /**
     * Instantiates a new error message.
     */
    private ErrorDescriptionConstants() {

    }

}
