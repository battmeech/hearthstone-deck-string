/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config;

/**
 * Messages used to create .info level log messages.
 */
public class HearthstoneDeckStringLogMessages {

    /** Service Started. */
    public static final String SERVICE_STARTED = "001 - Service started";

    /** Service Stopped. */
    public static final String SERVICE_STOPPED = "002 - Service stopped";

    /** Deck string request received. */
    public static final String DECK_STRING_REQUEST_RECEIVED = "003 - Deck string request received";

    /** Deck string response sent. */
    public static final String DECK_STRING_RESPONSE_SENT = "004 - Deck string response sent";

    /** Deck request received. */
    public static final String DECK_REQUEST_RECEIVED = "005 - Deck request received";

    /** Deck response sent. */
    public static final String DECK_RESPONSE_SENT = "006 - Deck response sent";

    /** Deck validation failed. */
    public static final String DECK_VALIDATION_FAILED = "007 - Deck failed validation";

    /** Card not found. */
    public static final String CARD_NOT_FOUND = "008 - Card not found";

    /** Error message sent. */
    public static final String ERROR_TO_CONSUMER = "009 - Error sent to consumer";

    /** Random deck request received. */
    public static final String RANDOM_DECK_STRING_RECEIVED = "010 - Random deck request received";

    /** Random deck response sent. */
    public static final String RANDOM_DECK_RESPONSE_SENT = "011 - Random deck response sent";

    /**
     * Instantiates a new hearthstone deck string log messages.
     */
    private HearthstoneDeckStringLogMessages() {

    }
}
