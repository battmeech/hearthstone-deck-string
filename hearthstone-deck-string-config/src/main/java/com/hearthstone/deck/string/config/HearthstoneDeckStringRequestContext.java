/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.config;

import java.util.UUID;

import javax.enterprise.context.RequestScoped;

/**
 * A class for storing a unique reference, which can be called for logs.
 */
@RequestScoped
public class HearthstoneDeckStringRequestContext {

    /**
     * The unique request reference.
     */
    private String uniqueRequestReference;

    /**
     * Gets the unique request reference.
     *
     * @return the unique request reference
     */
    public String getUniqueRequestReference() {
        return uniqueRequestReference;
    }

    /**
     * Sets the unique request reference.
     */
    public void setUniqueRequestReference() {
        final UUID uuid = UUID.randomUUID();
        this.uniqueRequestReference = uuid.toString();
    }

}
