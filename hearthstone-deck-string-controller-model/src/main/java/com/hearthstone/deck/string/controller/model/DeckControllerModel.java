/**
 * MBeech 2018
 */
package com.hearthstone.deck.string.controller.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * The deck controller model.
 */
@XmlRootElement(name = "hearthstoneDeck")
public class DeckControllerModel {

    /** The starting hero. */
    @XmlElement(name = "startingHero")
    @NotNull
    private String startingHero;

    /** The format. */
    @XmlElement(name = "format")
    @NotNull
    private String format;

    /** The one of cards. */
    @XmlElement(name = "oneOfCards")
    @NotNull
    private List<Integer> oneOfCards;

    /** The two of cards. */
    @XmlElement(name = "twoOfCards")
    @NotNull
    private List<Integer> twoOfCards;

    /**
     * Gets the starting hero.
     *
     * @return the starting hero
     */
    public String getStartingHero() {
        return startingHero;
    }

    /**
     * Sets the starting hero.
     *
     * @param startingHero
     *            the new starting hero
     */
    public void setStartingHero(final String startingHero) {
        this.startingHero = startingHero;
    }

    /**
     * Gets the format.
     *
     * @return the format
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the format.
     *
     * @param format
     *            the new format
     */
    public void setFormat(final String format) {
        this.format = format;
    }

    /**
     * Gets the one of cards.
     *
     * @return the one of cards
     */
    public List<Integer> getOneOfCards() {
        return oneOfCards;
    }

    /**
     * Sets the one of cards.
     *
     * @param oneOfCards
     *            the new one of cards
     */
    public void setOneOfCards(final List<Integer> oneOfCards) {
        this.oneOfCards = oneOfCards;
    }

    /**
     * Gets the two of cards.
     *
     * @return the two of cards
     */
    public List<Integer> getTwoOfCards() {
        return twoOfCards;
    }

    /**
     * Sets the two of cards.
     *
     * @param twoOfCards
     *            the new two of cards
     */
    public void setTwoOfCards(final List<Integer> twoOfCards) {
        this.twoOfCards = twoOfCards;
    }

    /**
     * @InheritDoc
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equal = false;

        if (obj instanceof DeckControllerModel) {
            final DeckControllerModel otherObj = (DeckControllerModel) obj;

            final EqualsBuilder equalsBuilder = new EqualsBuilder();

            equalsBuilder.append(startingHero, otherObj.startingHero).append(format, otherObj.format)
                    .append(oneOfCards, otherObj.oneOfCards).append(twoOfCards, otherObj.twoOfCards);

            equal = equalsBuilder.isEquals();
        }

        return equal;
    }

    /**
     * @InheritDoc
     */
    @Override
    public int hashCode() {
        final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();

        hashCodeBuilder.append(startingHero).append(format).append(oneOfCards).append(twoOfCards);

        final int hashCode = hashCodeBuilder.hashCode();

        return hashCode;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append(startingHero).append(format).append(oneOfCards).append(twoOfCards);

        return toStringBuilder.toString();
    }

}
