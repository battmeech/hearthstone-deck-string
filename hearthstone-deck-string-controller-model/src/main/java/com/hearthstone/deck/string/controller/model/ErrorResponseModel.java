/**
 * MBeech 2018
 */
package com.hearthstone.deck.string.controller.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Error response model
 */
@XmlRootElement(name = "hearthstoneDeckStringErrorResponse")
public class ErrorResponseModel {

    /** The message. */
    @XmlElement(name = "message")
    private String message;

    /** The description. */
    @XmlElement(name = "description")
    private String description;

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @InheritDoc
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equal = false;

        if (obj instanceof DeckStringControllerModel) {
            final ErrorResponseModel otherObj = (ErrorResponseModel) obj;

            final EqualsBuilder equalsBuilder = new EqualsBuilder();

            equalsBuilder.append(message, otherObj.message).append(description, otherObj.description);

            equal = equalsBuilder.isEquals();
        }

        return equal;
    }

    /**
     * @InheritDoc
     */
    @Override
    public int hashCode() {
        final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();

        hashCodeBuilder.append(message).append(description);

        final int hashCode = hashCodeBuilder.hashCode();

        return hashCode;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append(message).append(description);

        return toStringBuilder.toString();
    }

}
