/**
 * MBeech 2018
 */
package com.hearthstone.deck.string.controller.model;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Model which is returned when doing a post request, or random deck request.
 * Also used when requesting to transform a deck string into deck object.
 */
@XmlRootElement(name = "hearhstoneDeckString")
public class DeckStringControllerModel {

    /** The deck string. */
    @XmlElement(name = "deckString")
    @NotNull
    private String deckString;

    /**
     * Gets the deck string.
     *
     * @return the deck string
     */
    public String getDeckString() {
        return deckString;
    }

    /**
     * Sets the deck string.
     *
     * @param deckString
     *            the new deck string
     */
    public void setDeckString(final String deckString) {
        this.deckString = deckString;
    }

    /**
     * @InheritDoc
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equal = false;

        if (obj instanceof DeckStringControllerModel) {
            final DeckStringControllerModel otherObj = (DeckStringControllerModel) obj;

            final EqualsBuilder equalsBuilder = new EqualsBuilder();

            equalsBuilder.append(deckString, otherObj.deckString);

            equal = equalsBuilder.isEquals();
        }

        return equal;
    }

    /**
     * @InheritDoc
     */
    @Override
    public int hashCode() {
        final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();

        hashCodeBuilder.append(deckString);

        final int hashCode = hashCodeBuilder.hashCode();

        return hashCode;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append(deckString);

        return toStringBuilder.toString();
    }
}
