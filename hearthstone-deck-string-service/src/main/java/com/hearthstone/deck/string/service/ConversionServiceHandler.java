/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.exception.GenericException;
import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Deck.DeckBuilder;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;
import com.hearthstone.deck.string.domain.VarInt;

/**
 * Handler for {@link ConversionService}
 */
public class ConversionServiceHandler implements ConversionService {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(ConversionServiceHandler.class);

    /** The card reader service. */
    private final CardEnrichmentService cardEnrichmentService;

    /** The card producer service. */
    private final CardProducerService cardProducerService;

    /**
     * Instantiates a new conversion service handler.
     *
     * @param cardEnrichmentService
     *            the card enrichment service
     */
    @Inject
    public ConversionServiceHandler(final CardEnrichmentService cardEnrichmentService,
            final CardProducerService cardProducerService) {
        this.cardEnrichmentService = cardEnrichmentService;
        this.cardProducerService = cardProducerService;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String convertDeckToDeckString(final Deck deck) {

        ByteArrayOutputStream baos = null;
        DataOutputStream dos = null;

        try {
            baos = new ByteArrayOutputStream();
            dos = new DataOutputStream(baos);

            // Reserved byte always zero
            VarInt.writeVarInt(dos, 0);
            // Encoding version number
            VarInt.writeVarInt(dos, 1);
            // Deck format (standard/wild)
            VarInt.writeVarInt(dos, deck.getFormat().getFormatId());
            // Number of heroes in heroes array, always 1
            VarInt.writeVarInt(dos, 1);
            // Class being played
            VarInt.writeVarInt(dos, deck.getStartingHero().getHeroId());
            // Number of singletons
            VarInt.writeVarInt(dos, deck.getOneOfCards().size());

            // Iterate through each singleton card
            for (final Card card : deck.getOneOfCards()) {
                VarInt.writeVarInt(dos, card.getDbfId());
            }

            // Number of dual cards
            VarInt.writeVarInt(dos, deck.getTwoOfCards().size());

            // Iterate through each dual card
            for (final Card card : deck.getTwoOfCards()) {
                VarInt.writeVarInt(dos, card.getDbfId());
            }

            // The number of cards that have quantity greater than 2.
            // TODO add more than 2 card variable to Deck Object so this can handle arena
            // decks.
            VarInt.writeVarInt(dos, 0);

            // Flushes the output stream to the byte array output stream
            dos.flush();

            if (baos != null) {
                baos.close();
            }
            if (dos != null) {
                dos.close();
            }
        } catch (final IOException e) {
            LOG.debug("Exception caught {}", e);
            throw new GenericException(
                    new WebApplicationException("Internal Server Error", HttpStatus.SC_INTERNAL_SERVER_ERROR),
                    "Error when writing deck string.");
        }

        final String deckString = Base64.getEncoder().encodeToString(baos.toByteArray());
        return deckString;
    }

    /**
     * @InheritDoc
     */
    @Override
    public Deck decodeFromString(final String deckString) throws IOException {

        final DeckBuilder deckBuilder = new DeckBuilder();

        final byte[] deckBytes = Base64.getDecoder().decode(deckString);

        final List<Integer> deckNumbers = decodeBytesToInts(deckBytes);

        // Can skip to 2 as 0 is a reserved byte and 1 is the encoding version which
        // stays the same.
        int numbersElement = 2;

        // 2 represents the format. Can also skip 3 as it represents "number of heroes"
        // which will always be 1.
        deckBuilder.withFormat(Format.findValueFromId(deckNumbers.get(numbersElement)));
        numbersElement = numbersElement + 2;

        // 4th number is the starting hero ID
        deckBuilder.withStartingHero(StartingHero.findValueFromId(deckNumbers.get(numbersElement)));
        numbersElement++;

        // 5th number is amount of singleton cards in the deck
        final int oneOfSize = deckNumbers.get(numbersElement);
        numbersElement++;

        final Card[] cards = cardProducerService.produceCards();

        // Go through singleton cards and add each to deck object.
        final List<Card> oneOfcards = new ArrayList<Card>();
        for (int i = 0; i < oneOfSize; i++) {
            final Card card = cardEnrichmentService.findCardInJson(deckNumbers.get(numbersElement++), cards);
            oneOfcards.add(card);
        }
        deckBuilder.withOneOfCards(oneOfcards);

        // Number of duplicates in the deck
        final int twoOfSize = deckNumbers.get(numbersElement++);

        // Go through two of cards and add each to deck object.
        final List<Card> twoOfcards = new ArrayList<Card>();
        for (int i = 0; i < twoOfSize; i++) {
            final Card card = cardEnrichmentService.findCardInJson(deckNumbers.get(numbersElement++), cards);
            twoOfcards.add(card);
        }
        deckBuilder.withTwoOfCards(twoOfcards);

        return deckBuilder.build();

    }

    /**
     * Convert the byte array into a list of ints.
     *
     * @param bytes
     *            the bytes
     * @return the list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private List<Integer> decodeBytesToInts(final byte[] bytes) throws IOException {
        final DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bytes));
        return VarInt.readVarInt(dataInputStream);
    }
}
