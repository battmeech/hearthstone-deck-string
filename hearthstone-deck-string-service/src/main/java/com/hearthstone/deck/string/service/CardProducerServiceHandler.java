/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hearthstone.deck.string.config.ErrorDescriptionConstants;
import com.hearthstone.deck.string.config.HearthstoneDeckStringConfig;
import com.hearthstone.deck.string.config.HearthstoneDeckStringConstants;
import com.hearthstone.deck.string.config.HearthstoneJsonMethod;
import com.hearthstone.deck.string.config.exception.GenericException;
import com.hearthstone.deck.string.domain.Card;

/**
 * Handler for {@link CardProducerService}.
 */
public class CardProducerServiceHandler implements CardProducerService {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(CardProducerServiceHandler.class);

    /** The hearthstone deck string config. */
    private final HearthstoneDeckStringConfig hearthstoneDeckStringConfig;

    /**
     * Instantiates a new hearthstone json producer service handler.
     *
     * @param hearthstoneDeckStringConfig
     *            the hearthstone deck string config
     * @param cardContainer
     *            the card container
     */
    @Inject
    public CardProducerServiceHandler(final HearthstoneDeckStringConfig hearthstoneDeckStringConfig) {
        this.hearthstoneDeckStringConfig = hearthstoneDeckStringConfig;
    }

    /**
     * @InheritDoc
     */
    @Override
    public Card[] produceCards() {

        final HearthstoneJsonMethod jsonMethod = hearthstoneDeckStringConfig.getJsonMethod();
        LOG.debug("JSON acquisition method is {}", jsonMethod);

        final ObjectMapper mapper = new ObjectMapper();
        if (jsonMethod == HearthstoneJsonMethod.FILE) {
            try {
                final String fileLocation = hearthstoneDeckStringConfig.getJsonFileLocation();

                // Read in the file.
                final File file = new File(fileLocation);

                // Convert JSON string from file to Object
                LOG.debug("Reading in JSON file at {}", fileLocation);
                final Card[] cards = mapper.readValue(new FileReader(file), Card[].class);
                LOG.debug("Reading file at {} was successful.", fileLocation);
                return cards;
            } catch (final IOException e) {
                LOG.debug("Exception occured while reading hearthstone JSON {}", e);
                throw new GenericException(
                        new WebApplicationException("Internal Server Error", HttpStatus.SC_INTERNAL_SERVER_ERROR),
                        ErrorDescriptionConstants.JSON_ERROR);
            }

        } else {
            final String jsonUrl = hearthstoneDeckStringConfig.getJsonUrl();

            // Send a request to the hearthstone JSON api.
            final HttpClient client = new HttpClient();
            final GetMethod method = new GetMethod(jsonUrl);

            // Build a header so the request isn't rejected.
            final Header header = new Header();
            header.setName(HearthstoneDeckStringConstants.USER_AGENT_HEADER_KEY);
            header.setValue(HearthstoneDeckStringConstants.USER_AGENT_HEADER_VALUE);
            method.addRequestHeader(header);

            byte[] response = null;
            try {
                // Execute the request.
                LOG.debug("Sending request to {}", jsonUrl);
                final int statusCode = client.executeMethod(method);
                if (statusCode != HttpStatus.SC_OK) {
                    LOG.debug("Status code from API was {}", statusCode);
                    throw new GenericException(
                            new WebApplicationException("Internal Server Error", HttpStatus.SC_INTERNAL_SERVER_ERROR),
                            String.format(ErrorDescriptionConstants.INVALID_STATUS_CODE, statusCode));
                }

                LOG.debug("Request to {} successful", jsonUrl);
                response = method.getResponseBody();
            } catch (final IOException e) {
                LOG.debug("Exception occured while reading hearthstone JSON {}", e);
                throw new GenericException(
                        new WebApplicationException("Internal Server Error", HttpStatus.SC_INTERNAL_SERVER_ERROR),
                        ErrorDescriptionConstants.API_ERROR);
            }
            final String json = new String(response);

            // Convert JSON string from URL to Object
            Card[] cards;
            try {
                cards = mapper.readValue(json, Card[].class);
                return cards;
            } catch (final IOException e) {
                LOG.debug("Exception occured while reading hearthstone JSON {}", e);
                throw new GenericException(
                        new WebApplicationException("Internal Server Error", HttpStatus.SC_INTERNAL_SERVER_ERROR),
                        ErrorDescriptionConstants.JSON_ERROR);
            }

        }

    }

}
