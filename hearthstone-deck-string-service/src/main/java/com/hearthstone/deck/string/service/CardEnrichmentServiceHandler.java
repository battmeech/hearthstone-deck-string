/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.ErrorDescriptionConstants;
import com.hearthstone.deck.string.config.HearthstoneDeckStringLogMessages;
import com.hearthstone.deck.string.config.HearthstoneDeckStringRequestContext;
import com.hearthstone.deck.string.config.exception.CardNotFoundException;
import com.hearthstone.deck.string.domain.Card;

/**
 * Handler for {@link CardEnrichmentService}.
 */
public class CardEnrichmentServiceHandler implements CardEnrichmentService {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(CardEnrichmentServiceHandler.class);

    /** The Hearthstone deck string request context. */
    private final HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext;

    /**
     * Instantiates a new card enrichment service handler.
     *
     * @param hearthstoneDeckStringConfig
     *            the hearthstone deck string config
     * @param hearthstoneDeckStringRequestContext
     *            the hearthstone deck string request context
     */
    @Inject
    public CardEnrichmentServiceHandler(final HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext) {
        this.hearthstoneDeckStringRequestContext = hearthstoneDeckStringRequestContext;
    }

    /**
     * @InheritDoc
     */
    @Override
    public Card findCardInJson(final int dbfId, final Card[] cards) {

        // Go through each card and find the one that matches.
        for (final Card card : cards) {
            if (card.getDbfId() == dbfId) {
                LOG.debug("Found card in JSON. ", card.getName(), card.getDbfId());
                return card;
            }
        }

        LOG.info(String.format("%s - %s - %s", HearthstoneDeckStringLogMessages.CARD_NOT_FOUND,
                hearthstoneDeckStringRequestContext.getUniqueRequestReference(), dbfId));
        throw new CardNotFoundException(new WebApplicationException("Card not found.", HttpStatus.SC_BAD_REQUEST),
                String.format(ErrorDescriptionConstants.CARD_NOT_FOUND, dbfId));
    }
}
