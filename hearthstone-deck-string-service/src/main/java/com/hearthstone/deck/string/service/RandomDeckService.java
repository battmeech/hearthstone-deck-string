/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;

/**
 * A service which creates random and valid decks for people to try.
 */
public interface RandomDeckService {

    /**
     * Generate a random deck with no constraints. Will be any class in either
     * format.
     *
     * @return the deck
     */
    public Deck generateRandomDeck();

    /**
     * Generate a random deck where the hero is pre-chosen.
     *
     * @param startingHero
     *            the starting hero
     * @return the deck
     */
    public Deck generateRandomDeck(final StartingHero startingHero);

    /**
     * Generate a random deck where the format is pre-chosen.
     *
     * @param format
     *            the format
     * @return the deck
     */
    public Deck generateRandomDeck(final Format format);

    /**
     * Generate a random deck where the hero and format is pre-chosen. Boring!
     *
     * @param startingHero
     *            the starting hero
     * @param format
     *            the format
     * @return the deck
     */
    public Deck generateRandomDeck(final StartingHero startingHero, final Format format);
}
