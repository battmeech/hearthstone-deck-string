/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import java.io.IOException;

import com.hearthstone.deck.string.domain.Deck;

/**
 * Conversion Service used for converting decks to strings, and vice verse.
 *
 */
public interface ConversionService {

    /**
     * Take in a Deck object, and convert this into the deck string.
     *
     * @param deck
     *            the deck
     * @return the string
     */
    public String convertDeckToDeckString(final Deck deck);

    /**
     * Decode the deck string and convert into a Deck object.
     *
     * @param deckString
     *            the deck string
     * @return the deck
     * @throws IOException
     */
    public Deck decodeFromString(final String deckString) throws IOException;
}
