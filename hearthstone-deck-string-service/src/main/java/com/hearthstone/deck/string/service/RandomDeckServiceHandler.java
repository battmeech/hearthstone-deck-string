/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.HearthstoneDeckStringConstants;
import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Expansion;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.MultiClassGroup;
import com.hearthstone.deck.string.domain.StartingHero;

/**
 * Handler for {@link RandomDeckService}
 */
public class RandomDeckServiceHandler implements RandomDeckService {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(RandomDeckServiceHandler.class);

    /** Static list of heroes. */
    private static final List<StartingHero> HEROES = Arrays.asList(StartingHero.values());

    /** Static list of formats. */
    private static final List<Format> FORMATS = Arrays.asList(Format.values());

    /** The card producer service. */
    private final CardProducerService cardProducerService;

    /**
     * Instantiates a new random deck service handler.
     *
     * @param cardProducerService
     *            the card producer service
     */
    @Inject
    public RandomDeckServiceHandler(final CardProducerService cardProducerService) {
        this.cardProducerService = cardProducerService;
    }

    /**
     * @InheritDoc
     */
    @Override
    public Deck generateRandomDeck() {
        return generateRandomDeck(null, null);

    }

    /**
     * @InheritDoc
     */
    @Override
    public Deck generateRandomDeck(final StartingHero startingHero) {
        return generateRandomDeck(startingHero, null);
    }

    /**
     * @InheritDoc
     */
    @Override
    public Deck generateRandomDeck(final Format format) {
        return generateRandomDeck(null, format);
    }

    /**
     * @InheritDoc
     */
    @Override
    public Deck generateRandomDeck(StartingHero startingHero, Format format) {
        // Determine the amount of one of and two of cards.
        final int numTwoOfCards = new Random().nextInt(16);
        final int numOneOfCards = 30 - (numTwoOfCards * 2);

        // Check if starting hero has a value currently, if not assign it randomly.
        if (startingHero == null) {
            final int heroSelector = new Random().nextInt(HEROES.size());
            startingHero = HEROES.get(heroSelector);
        }
        final String heroClass = startingHero.getHeroClass();

        // Check if format has a value currently, if not assign it randomly.
        final int formatSelector = new Random().nextInt(2) + 1;
        if (format == null) {
            format = FORMATS.get(formatSelector);
        }

        LOG.debug("Starting random deck generation for a {} {} deck.", format.getFormatName(),
                startingHero.getHeroClass());

        // Get the available expansions for this format.
        final List<Expansion> availableExpansions = new ArrayList<Expansion>();
        for (final Expansion expansion : Expansion.values()) {
            if (expansion.getExpansionFormat().contains(format)) {
                availableExpansions.add(expansion);
            }
        }

        // Produce a list of cards which can be used in this random generation.
        final Card[] cardsFromJson = cardProducerService.produceCards();
        final List<Card> availableCards = Arrays.asList(cardsFromJson).stream()
                .filter(c -> (c.getCardClass().equalsIgnoreCase(heroClass)
                        || c.getCardClass().equalsIgnoreCase(HearthstoneDeckStringConstants.NEUTRAL_CARD))
                        && availableExpansions.contains(Expansion.getExpansionFromCode(c.getSet()))
                        && checkMultiClass(c, heroClass) && !StartingHero.isHeroClass(c.getDbfId()))
                .collect(Collectors.toList());

        final List<Integer> cardSelectors = new ArrayList<Integer>();

        // Generate one of cards.
        final List<Card> oneOfCards = new ArrayList<Card>();
        for (int index = 0; index < numOneOfCards; index++) {
            final int cardSelector = new Random().nextInt(availableCards.size());

            // To prevent the randomness from picking the same number twice.
            if (cardSelectors.contains(cardSelector)) {
                index--;
                continue;
            }
            cardSelectors.add(cardSelector);
            final Card card = availableCards.get(cardSelector);
            oneOfCards.add(card);
        }

        // Generate two of cards.
        final List<Card> twoOfCards = new ArrayList<Card>();
        for (int index = 0; index < numTwoOfCards; index++) {
            final int cardSelector = new Random().nextInt(availableCards.size());

            // To prevent the randomness from picking the same number twice.
            if (cardSelectors.contains(cardSelector)) {
                index--;
                continue;
            }
            cardSelectors.add(cardSelector);

            final Card card = availableCards.get(cardSelector);
            // Check there are no legendary cards in the two of section.
            if (card.getRarity().equalsIgnoreCase(HearthstoneDeckStringConstants.LEGENDARY_CARD)) {
                index--;
                continue;
            }

            twoOfCards.add(card);
        }

        final Deck deck = new Deck.DeckBuilder().withFormat(format).withStartingHero(startingHero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards).build();
        LOG.debug("Finished Deck {}", deck);
        return deck;

    }

    /**
     * If a card contains a multi class group, validate the the hero is one of those
     * in the multi class. Return true if the current card has a multi class and the
     * decks starting class is one of the heroes, return false if there is no multi
     * class or if the class is not in the multi class group
     *
     * @param card
     *            the card
     * @param startingHero
     *            the starting hero
     * @return true, if successful
     */
    private boolean checkMultiClass(final Card card, final String startingHero) {
        if (card.getMultiClassGroup() != null) {
            final MultiClassGroup multiClassGroup = MultiClassGroup.findFromClassGroup(card.getMultiClassGroup());
            if (!multiClassGroup.getClassesInGroup().contains(startingHero)) {
                return false;
            }
        }
        return true;
    }

}
