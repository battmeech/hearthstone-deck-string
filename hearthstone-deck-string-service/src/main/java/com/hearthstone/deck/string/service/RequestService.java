/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import java.io.IOException;

import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.request.RandomRequest;

/**
 * The service which handles incoming requests.
 */
public interface RequestService {

    /**
     * Process an incoming deck string and return a deck.
     *
     * @param deckString
     *            the deck string
     * @return the deck
     * @throws IOException
     */
    public Deck processDeckString(final String deckString) throws IOException;

    /**
     * Process a deck and return a deck string.
     *
     * @param deck
     *            the deck
     * @return the string
     */
    public String processDeck(final Deck deck);

    /**
     * Produce a random deck for the consumer.
     *
     * @return the string
     */
    public String randomDeck(final RandomRequest randomRequest);

}
