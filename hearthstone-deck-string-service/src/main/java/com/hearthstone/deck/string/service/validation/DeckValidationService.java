/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service.validation;

import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.ValidationResult;

/**
 * The deck validation service. This contains methods which will return a
 * validation result.
 */
public interface DeckValidationService {

    /**
     * Validate the deck which is passed through. This should check length, that
     * each card is from the correct class, it should also look at the format of
     * each card and check that they are appropriate to the format given. Will also
     * need to handle tri-class cards.
     *
     * @param deck
     *            the deck
     * @return the validation result
     */
    public ValidationResult validateDeck(Deck deck);

}
