/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service.validation;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.ErrorDescriptionConstants;
import com.hearthstone.deck.string.config.HearthstoneDeckStringConstants;
import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Expansion;
import com.hearthstone.deck.string.domain.MultiClassGroup;
import com.hearthstone.deck.string.domain.StartingHero;
import com.hearthstone.deck.string.domain.ValidationResult;
import com.hearthstone.deck.string.domain.ValidationStatus;

/**
 * Handler for {@link DeckValidationService}
 */
public class DeckValidationServiceHandler implements DeckValidationService {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(DeckValidationServiceHandler.class);

    /**
     * @InheritDoc
     */
    @Override
    public ValidationResult validateDeck(final Deck deck) {

        final ValidationResult validationResult = new ValidationResult();

        final List<String> message = new ArrayList<String>();

        // First validation
        validateDeckLength(deck, message, validationResult);

        // Second validation
        validateCardsAreFromCorrectClass(deck, message, validationResult);

        // Third validation
        validateNoDuplicateLegendary(deck, message, validationResult);

        // Fourth validation
        validateFormat(deck, message, validationResult);

        // Fifth validation
        validateNoHeroes(deck, message, validationResult);

        validationResult.setMessage(message);

        return validationResult;
    }

    /**
     * Validate the length of a deck. Valid decks should contain 30 cards. Any deck
     * over this will be invalid and should throw an error, however decks under this
     * can still be considered valid, but have an incomplete message.
     *
     * @param deck
     *            the deck
     * @param stringBuilder
     *            the string builder
     * @param validationResult
     *            the validation result
     */
    private void validateDeckLength(final Deck deck, final List<String> message,
            final ValidationResult validationResult) {
        int deckCount = 0;

        for (int index = 0; index < deck.getOneOfCards().size(); index++) {
            deckCount++;
        }

        for (int index = 0; index < deck.getTwoOfCards().size(); index++) {
            deckCount += 2;
        }

        if (deckCount == 30) {
            validationResult.setValidationStatus(ValidationStatus.VALID);
        } else if (deckCount < 30) {
            LOG.debug("Deck was incomplete only contained {} cards.", deckCount);
            validationResult.setValidationStatus(ValidationStatus.INCOMPLETE);
            message.add(String.format(ErrorDescriptionConstants.TOO_FEW_CARDS, deckCount));
        } else if (deckCount > 30) {
            LOG.debug("Deck had too many cards -  contained {}.", deckCount);
            validationResult.setValidationStatus(ValidationStatus.INVALID);
            message.add(String.format(ErrorDescriptionConstants.TOO_MANY_CARDS, deckCount));
        }

    }

    /**
     * Validate each of the cards in the deck can be played by the class.
     *
     * @param deck
     *            the deck
     * @param stringBuilder
     *            the string builder
     * @param validationResult
     *            the validation result
     */
    private void validateCardsAreFromCorrectClass(final Deck deck, final List<String> message,
            final ValidationResult validationResult) {

        boolean deckValid = true;

        // Check the singleton cards first to see which don't fit the class.
        for (final Card card : deck.getOneOfCards()) {
            if (!card.getCardClass().equalsIgnoreCase(deck.getStartingHero().getHeroClass())
                    && !card.getCardClass().equals(HearthstoneDeckStringConstants.NEUTRAL_CARD)
                    || !checkMultiClassGroup(card, deck.getStartingHero())) {
                LOG.debug("{} does not belong to {}", card.getName(), deck.getStartingHero().getHeroClass());
                deckValid = false;
                message.add(String.format(ErrorDescriptionConstants.INVALID_CLASS_CARD, card.getName(),
                        deck.getStartingHero().getHeroClass()));
            }
        }

        // Rinse and repeat with two of cards.
        for (final Card card : deck.getTwoOfCards()) {
            if (!card.getCardClass().equalsIgnoreCase(deck.getStartingHero().getHeroClass())
                    && !card.getCardClass().equals(HearthstoneDeckStringConstants.NEUTRAL_CARD)
                    || !checkMultiClassGroup(card, deck.getStartingHero())) {
                deckValid = false;
                LOG.debug("{} does not belong to {}", card.getName(), deck.getStartingHero().getHeroClass());
                message.add(String.format(ErrorDescriptionConstants.INVALID_CLASS_CARD, card.getName(),
                        deck.getStartingHero().getHeroClass()));
            }
        }

        // Check if the deck remained valid in this stage.
        if (!deckValid) {
            validationResult.setValidationStatus(ValidationStatus.INVALID);
        }
    }

    /**
     * Validate there are no duplicate legendaries.
     *
     * @param deck
     *            the deck
     * @param stringBuilder
     *            the string builder
     * @param validationResult
     *            the validation result
     */
    private void validateNoDuplicateLegendary(final Deck deck, final List<String> message,
            final ValidationResult validationResult) {

        // Go through the two of cards and ensure there's no legendaries.
        for (final Card card : deck.getTwoOfCards()) {
            if (card.getRarity().equalsIgnoreCase(HearthstoneDeckStringConstants.LEGENDARY_CARD)) {
                LOG.debug("Deck contains two of {}", card.getName());
                message.add(String.format(ErrorDescriptionConstants.DUPLICATE_LEGENDARY, card.getName()));
                validationResult.setValidationStatus(ValidationStatus.INVALID);
            }
        }
    }

    /**
     * Validate the cards are allowed to be played in standard if the deck is
     * supposedly a standard deck.
     *
     * @param deck
     *            the deck
     * @param stringBuilder
     *            the string builder
     * @param validationResult
     *            the validation result
     */
    private void validateFormat(final Deck deck, final List<String> message, final ValidationResult validationResult) {
        // Go through each one of card and make sure they all belong in the selected
        // format.
        for (final Card card : deck.getOneOfCards()) {
            final Expansion expansion = Expansion.getExpansionFromCode(card.getSet());
            if (!expansion.getExpansionFormat().contains(deck.getFormat())) {
                LOG.debug("{} cannot be played in {}.", card.getName(), deck.getFormat());
                validationResult.setValidationStatus(ValidationStatus.INVALID);
                message.add(String.format(ErrorDescriptionConstants.INVALID_CARD_FORMAT, card.getName()));
            }
        }
        // Go through each two of card and make sure they all belong in selected format.
        for (final Card card : deck.getTwoOfCards()) {
            final Expansion expansion = Expansion.getExpansionFromCode(card.getSet());
            if (!expansion.getExpansionFormat().contains(deck.getFormat())) {
                LOG.debug("{} cannot be played in {}.", card.getName(), deck.getFormat());
                validationResult.setValidationStatus(ValidationStatus.INVALID);
                message.add(String.format(ErrorDescriptionConstants.INVALID_CARD_FORMAT, card.getName()));
            }
        }
    }

    /**
     * If a card contains a multi class group, validate the the hero is one of those
     * in the multi class. Return true if the current card has a multi class and the
     * decks starting class is one of the heroes, return false if there is no multi
     * class or if the class is not in the multi class group
     *
     * @param card
     *            the card
     * @param startingHero
     *            the starting hero
     * @return true, if successful
     */
    private boolean checkMultiClassGroup(final Card card, final StartingHero startingHero) {
        if (card.getMultiClassGroup() != null) {
            LOG.debug("Multi class group found - {} Hero class is - {}", card.getMultiClassGroup(),
                    startingHero.getHeroClass());
            final MultiClassGroup multiClassGroup = MultiClassGroup.findFromClassGroup(card.getMultiClassGroup());
            if (!multiClassGroup.getClassesInGroup().contains(startingHero.getHeroClass())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validate that dbfId in the deck are actually the IDs of a hero class.
     *
     * @param deck
     *            the deck
     * @param message
     *            the message
     * @param validationResult
     *            the validation result
     */
    private void validateNoHeroes(final Deck deck, final List<String> message,
            final ValidationResult validationResult) {
        for (final Card card : deck.getOneOfCards()) {
            if (StartingHero.isHeroClass(card.getDbfId())) {
                message.add(String.format(ErrorDescriptionConstants.HERO_IN_DECK, card.getName()));
                validationResult.setValidationStatus(ValidationStatus.INVALID);
            }
        }
        for (final Card card : deck.getTwoOfCards()) {
            if (StartingHero.isHeroClass(card.getDbfId())) {
                message.add(String.format(ErrorDescriptionConstants.HERO_IN_DECK, card.getName()));
                validationResult.setValidationStatus(ValidationStatus.INVALID);
            }
        }

    }
}
