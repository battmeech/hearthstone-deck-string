/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import com.hearthstone.deck.string.domain.Card;

/**
 * Card Enrichment Service. Reads the Hearthstone JSON to determine properties
 * about each card and enrich the data on each one.
 *
 */
public interface CardEnrichmentService {

    /**
     * Find card in json.
     *
     * @param dfId
     *            the df id
     * @return the card
     */
    public Card findCardInJson(final int dbfId, final Card[] cards);

}
