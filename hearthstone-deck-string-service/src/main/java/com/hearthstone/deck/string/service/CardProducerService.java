/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import com.hearthstone.deck.string.domain.Card;

/**
 * The card producer service.
 */
public interface CardProducerService {

    /**
     * Produce an array of cards. This can either be from a file or directly from
     * the hearthstone card API.
     *
     * @return the card[]
     */
    public Card[] produceCards();

}
