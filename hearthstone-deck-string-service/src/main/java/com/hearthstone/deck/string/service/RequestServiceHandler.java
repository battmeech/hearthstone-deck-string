/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.HearthstoneDeckStringConfig;
import com.hearthstone.deck.string.config.HearthstoneDeckStringLogMessages;
import com.hearthstone.deck.string.config.HearthstoneDeckStringRequestContext;
import com.hearthstone.deck.string.config.exception.InvalidDeckException;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.ValidationResult;
import com.hearthstone.deck.string.domain.ValidationStatus;
import com.hearthstone.deck.string.domain.request.RandomRequest;
import com.hearthstone.deck.string.service.validation.DeckValidationService;

/**
 * Handler for {@link RequestService}
 */
public class RequestServiceHandler implements RequestService {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(RequestServiceHandler.class);

    /** The conversion service. */
    private final ConversionService conversionService;

    /** The deck validation service. */
    private final DeckValidationService deckValidationService;

    /** The request context. */
    private final HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext;

    /** The hearthstone deck string config. */
    private final HearthstoneDeckStringConfig hearthstoneDeckStringConfig;

    /** The random deck service. */
    private final RandomDeckService randomDeckService;

    /**
     * Instantiates a new request service handler.
     *
     * @param conversionService
     *            the conversion service
     * @param deckValidationService
     *            the deck validation service
     * @param hearthstoneDeckStringRequestContext
     *            the hearthstone deck string request context
     * @param hearthstoneDeckStringConfig
     *            the hearthstone deck string config
     * @param randomDeckService
     *            the random deck service
     */
    @Inject
    public RequestServiceHandler(final ConversionService conversionService,
            final DeckValidationService deckValidationService,
            final HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext,
            final HearthstoneDeckStringConfig hearthstoneDeckStringConfig, final RandomDeckService randomDeckService) {
        this.conversionService = conversionService;
        this.deckValidationService = deckValidationService;
        this.hearthstoneDeckStringRequestContext = hearthstoneDeckStringRequestContext;
        this.hearthstoneDeckStringConfig = hearthstoneDeckStringConfig;
        this.randomDeckService = randomDeckService;
    }

    /**
     * @InheritDoc
     */
    @Override
    public Deck processDeckString(final String deckString) throws IOException {

        LOG.debug("Processing deck string: {}", deckString);
        final Deck deck = conversionService.decodeFromString(deckString);

        if (hearthstoneDeckStringConfig.toValidate()) {
            LOG.debug("Deck validation is switched on.");
            final ValidationResult validationResult = deckValidationService.validateDeck(deck);

            if (validationResult.getValidationStatus() == ValidationStatus.INVALID) {

                // As the validation result can contain multiple message which describe what
                // went wrong, need to turn them into a readable string, and the default
                // toString() method doesn't do this too well.
                final StringBuilder stringBuilder = new StringBuilder();
                for (final String error : validationResult.getMessage()) {
                    stringBuilder.append(error);
                }

                final String validationReason = stringBuilder.toString();

                LOG.warn(String.format("%s - %s - %s", HearthstoneDeckStringLogMessages.DECK_VALIDATION_FAILED,
                        hearthstoneDeckStringRequestContext.getUniqueRequestReference(), validationReason));
                throw new InvalidDeckException(new WebApplicationException("Invalid Deck", HttpStatus.SC_BAD_REQUEST),
                        validationReason);
            }
        }
        return deck;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String processDeck(final Deck deck) {

        LOG.debug("Processing deck. {}", deck.toString());
        if (hearthstoneDeckStringConfig.toValidate()) {
            LOG.debug("Deck validation is switched on.");
            final ValidationResult validationResult = deckValidationService.validateDeck(deck);

            if (validationResult.getValidationStatus() == ValidationStatus.INVALID) {

                // As the validation result can contain multiple message which describe what
                // went wrong, need to turn them into a readable string, and the default
                // toString() method doesn't do this too well.
                final StringBuilder stringBuilder = new StringBuilder();
                for (final String error : validationResult.getMessage()) {
                    stringBuilder.append(error);
                }
                throw new InvalidDeckException(new WebApplicationException("Invalid Deck", HttpStatus.SC_BAD_REQUEST),
                        stringBuilder.toString());
            }
        }
        final String deckString = conversionService.convertDeckToDeckString(deck);

        return deckString;
    }

    /**
     * @InheritDoc
     */
    @Override
    public String randomDeck(final RandomRequest randomRequest) {

        LOG.debug("Producing a random deck");

        final Deck deck = randomDeckService.generateRandomDeck(randomRequest.getStartingHero(),
                randomRequest.getFormat());

        final ValidationResult validationResult = deckValidationService.validateDeck(deck);

        if (validationResult.getValidationStatus() == ValidationStatus.INVALID) {

            // As the validation result can contain multiple message which describe what
            // went wrong, need to turn them into a readable string, and the default
            // toString() method doesn't do this too well.
            final StringBuilder stringBuilder = new StringBuilder();
            for (final String error : validationResult.getMessage()) {
                stringBuilder.append(error);
            }
            throw new InvalidDeckException(new WebApplicationException("Invalid Deck", HttpStatus.SC_BAD_REQUEST),
                    stringBuilder.toString());
        }
        final String deckString = conversionService.convertDeckToDeckString(deck);
        return deckString;
    }
}
