/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service.test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;
import com.hearthstone.deck.string.domain.ValidationResult;
import com.hearthstone.deck.string.domain.ValidationStatus;
import com.hearthstone.deck.string.service.CardProducerService;
import com.hearthstone.deck.string.service.RandomDeckService;
import com.hearthstone.deck.string.service.RandomDeckServiceHandler;
import com.hearthstone.deck.string.service.validation.DeckValidationService;
import com.hearthstone.deck.string.service.validation.DeckValidationServiceHandler;

/**
 * Unit test for {@link RandomDeckServiceHandler}
 */
@RunWith(MockitoJUnitRunner.class)
public class RandomDeckServiceTest {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(RandomDeckServiceTest.class);

    /** Validator needed to validate test result. */
    private DeckValidationService validator;

    /** The card producer service. */
    @Mock
    private CardProducerService cardProducerService;

    private Card[] cards;

    /** Class under test. */
    private RandomDeckService randomDeckService;

    /**
     * Setup. @throws IOException @throws @throws @throws
     */
    @Before
    public void setup() throws IOException {
        validator = new DeckValidationServiceHandler();
        randomDeckService = new RandomDeckServiceHandler(cardProducerService);

        final ObjectMapper mapper = new ObjectMapper();
        final File file = new File(getClass().getClassLoader().getResource("hearthstone.json").getPath());

        cards = mapper.readValue(new FileReader(file), Card[].class);
    }

    /**
     * Test random deck generation.
     */
    @Test
    public void testDeckGenerationNoParameters() {

        // Mock
        Mockito.when(cardProducerService.produceCards()).thenReturn(cards);

        // Run test
        final Deck deckToTest = randomDeckService.generateRandomDeck();
        final ValidationResult result = validator.validateDeck(deckToTest);

        // Assert
        Assert.assertEquals(ValidationStatus.VALID, result.getValidationStatus());
        LOG.debug("Messages: {}", result.getMessage());

    }

    /**
     * Test deck generation with standard format.
     */
    @Test
    public void testDeckGenerationWithStandardFormat() {

        // Setup
        final Format expectedFormat = Format.FT_STANDARD;

        // Mock
        Mockito.when(cardProducerService.produceCards()).thenReturn(cards);

        // Run test
        final Deck deckToTest = randomDeckService.generateRandomDeck(expectedFormat);
        final ValidationResult result = validator.validateDeck(deckToTest);

        // Assert
        Assert.assertEquals(ValidationStatus.VALID, result.getValidationStatus());
        Assert.assertEquals(expectedFormat, deckToTest.getFormat());
        LOG.debug("Messages: {}", result.getMessage());

    }

    /**
     * Test deck generation with wild format.
     */
    @Test
    public void testDeckGenerationWithWildFormat() {

        // Setup
        final Format expectedFormat = Format.FT_WILD;

        // Mock
        Mockito.when(cardProducerService.produceCards()).thenReturn(cards);

        // Run test
        final Deck deckToTest = randomDeckService.generateRandomDeck(expectedFormat);
        final ValidationResult result = validator.validateDeck(deckToTest);

        // Assert
        Assert.assertEquals(ValidationStatus.VALID, result.getValidationStatus());
        Assert.assertEquals(expectedFormat, deckToTest.getFormat());
        LOG.debug("Messages: {}", result.getMessage());

    }

    /**
     * Test deck generation with priest set.
     */
    @Test
    public void testDeckGenerationWithPriest() {

        // Setup
        final StartingHero expectedHero = StartingHero.ANDUIN;

        // Mock
        Mockito.when(cardProducerService.produceCards()).thenReturn(cards);

        // Run test
        final Deck deckToTest = randomDeckService.generateRandomDeck(expectedHero);
        final ValidationResult result = validator.validateDeck(deckToTest);

        // Assert
        Assert.assertEquals(ValidationStatus.VALID, result.getValidationStatus());
        Assert.assertEquals(expectedHero, deckToTest.getStartingHero());

    }

    /**
     * Test deck generation with priest and wild format.
     */
    @Test
    public void testDeckGenerationWithFormatAndClass() {

        // Setup
        final StartingHero expectedHero = StartingHero.ANDUIN;
        final Format expectedFormat = Format.FT_WILD;

        // Mock
        Mockito.when(cardProducerService.produceCards()).thenReturn(cards);

        // Run test
        final Deck deckToTest = randomDeckService.generateRandomDeck(expectedHero, expectedFormat);
        final ValidationResult result = validator.validateDeck(deckToTest);

        // Assert
        Assert.assertEquals(ValidationStatus.VALID, result.getValidationStatus());
        Assert.assertEquals(expectedHero, deckToTest.getStartingHero());
        Assert.assertEquals(expectedFormat, deckToTest.getFormat());
        LOG.debug("Messages: {}", result.getMessage());

    }
}
