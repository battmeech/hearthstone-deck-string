/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service.test.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hearthstone.deck.string.config.ErrorDescriptionConstants;
import com.hearthstone.deck.string.config.HearthstoneDeckStringConstants;
import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Card.CardBuilder;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Deck.DeckBuilder;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;
import com.hearthstone.deck.string.domain.ValidationResult;
import com.hearthstone.deck.string.domain.ValidationStatus;
import com.hearthstone.deck.string.service.validation.DeckValidationService;
import com.hearthstone.deck.string.service.validation.DeckValidationServiceHandler;

/**
 * Unit tests for {@link DeckValidationService}
 */
public class DeckValidationServiceTest {

    /** The card rarity for testing. */
    private static final String CARD_RARITY = "FREE";

    /** The expansion for testing. */
    private static final String CARD_SET = "CORE";

    /** The legendary card name used for testing. */
    private static final String LEGENDARY_CARD_NAME = "The Boogeymonster";

    /** The deck validation service. */
    private DeckValidationService deckValidationService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        this.deckValidationService = new DeckValidationServiceHandler();
    }

    /**
     * Validation Result when the length is too long.
     */
    @Test
    public void checkValidDeck() {

        // Setup deck deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927, 43502);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        // Setup expected validation result.
        final List<String> message = new ArrayList<String>();
        final ValidationResult expectedValidationResult = new ValidationResult();
        expectedValidationResult.setValidationStatus(ValidationStatus.VALID);
        expectedValidationResult.setMessage(message);

        // Run test
        final ValidationResult actualValidationResult = deckValidationService.validateDeck(deck);

        // Assert
        Assert.assertEquals(expectedValidationResult, actualValidationResult);

    }

    /**
     * Validation Result when the length is too long.
     */
    @Test
    public void checkDeckTooLong() {

        // Setup deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419, 333);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927, 43502, 123);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        final List<String> message = new ArrayList<String>();
        message.add(String.format(" Deck contained %d cards, which is over the card limit!", 33));

        // Setup expected validation result.
        final ValidationResult expectedValidationResult = new ValidationResult();
        expectedValidationResult.setValidationStatus(ValidationStatus.INVALID);
        expectedValidationResult.setMessage(message);

        // Run test
        final ValidationResult actualValidationResult = deckValidationService.validateDeck(deck);

        // Assert
        Assert.assertEquals(expectedValidationResult, actualValidationResult);

    }

    /**
     * Check deck too short.
     */
    @Test
    public void checkDeckTooShort() {

        // Setup deck deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419, 333);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        final List<String> message = new ArrayList<String>();
        message.add(String.format(String.format("Deck only contained %s cards.", 9)));

        // Setup expected validation result.
        final ValidationResult expectedValidationResult = new ValidationResult();
        expectedValidationResult.setValidationStatus(ValidationStatus.INCOMPLETE);
        expectedValidationResult.setMessage(message);

        // Run test
        final ValidationResult actualValidationResult = deckValidationService.validateDeck(deck);

        // Assert
        Assert.assertEquals(expectedValidationResult, actualValidationResult);

    }

    /**
     * Check class validation fail.
     */
    @Test
    public void checkClassValidationFail() {

        // Setup deck deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927, 43502);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.GARROSH.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        // Setup expected validation result.
        final ValidationResult expectedValidationResult = new ValidationResult();
        expectedValidationResult.setValidationStatus(ValidationStatus.INVALID);

        // Run test
        final ValidationResult actualValidationResult = deckValidationService.validateDeck(deck);

        // Assert
        Assert.assertEquals(expectedValidationResult.getValidationStatus(),
                actualValidationResult.getValidationStatus());

    }

    /**
     * Check validation when there are multiple legendary cards.
     */
    @Test
    public void checkMultipleLegendary() {

        // Setup deck deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final CardBuilder cardBuilder = new CardBuilder().withDbfId(38895)
                .withCardClass(HearthstoneDeckStringConstants.NEUTRAL_CARD)
                .withRarity(HearthstoneDeckStringConstants.LEGENDARY_CARD).withSet(CARD_SET)
                .withName(LEGENDARY_CARD_NAME);
        twoOfCards.add(cardBuilder.build());

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        // Setup expected validation result.
        final ValidationResult expectedValidationResult = new ValidationResult();
        expectedValidationResult.setValidationStatus(ValidationStatus.INVALID);

        // Setup expected message.
        final List<String> message = new ArrayList<String>();
        message.add(String.format(ErrorDescriptionConstants.DUPLICATE_LEGENDARY, LEGENDARY_CARD_NAME));
        expectedValidationResult.setMessage(message);

        // Run test
        final ValidationResult actualValidationResult = deckValidationService.validateDeck(deck);

        // Assert
        Assert.assertEquals(expectedValidationResult, actualValidationResult);

    }

    /**
     * Check validation when a wild card is put in a standard deck.
     */
    @Test
    public void checkWrongFormat() {

        // Setup deck deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_STANDARD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final CardBuilder cardBuilder = new CardBuilder().withDbfId(38895)
                .withCardClass(HearthstoneDeckStringConstants.NEUTRAL_CARD).withRarity("Epic").withSet("GVG")
                .withName("Mechwarper");
        twoOfCards.add(cardBuilder.build());

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        // Setup expected validation result.
        final ValidationResult expectedValidationResult = new ValidationResult();
        expectedValidationResult.setValidationStatus(ValidationStatus.INVALID);

        // Setup expected message.
        final List<String> message = new ArrayList<String>();
        message.add(String.format(ErrorDescriptionConstants.INVALID_CARD_FORMAT, "Mechwarper"));

        expectedValidationResult.setMessage(message);

        // Run test
        final ValidationResult actualValidationResult = deckValidationService.validateDeck(deck);

        // Assert
        Assert.assertEquals(expectedValidationResult, actualValidationResult);

    }

    /**
     * Check validation when one card is in the wrong multi class.
     */
    @Test
    public void checkMultiClassInvalid() {

        // Setup deck deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final CardBuilder cardBuilder = new CardBuilder().withDbfId(38895)
                .withCardClass(HearthstoneDeckStringConstants.NEUTRAL_CARD).withRarity(CARD_RARITY).withSet(CARD_SET)
                .withName("Aya Blackpaw").withMultiClassGroup("JADE_LOTUS");
        twoOfCards.add(cardBuilder.build());

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        // Setup expected validation result.
        final ValidationResult expectedValidationResult = new ValidationResult();
        expectedValidationResult.setValidationStatus(ValidationStatus.INVALID);

        // Setup expected message.
        final List<String> message = new ArrayList<String>();
        message.add(String.format(ErrorDescriptionConstants.INVALID_CLASS_CARD, "Aya Blackpaw", hero.getHeroClass()));
        expectedValidationResult.setMessage(message);

        // Run test
        final ValidationResult actualValidationResult = deckValidationService.validateDeck(deck);

        // Assert
        Assert.assertEquals(expectedValidationResult, actualValidationResult);

    }

    /**
     * Validation test when only two ofs.
     */
    @Test
    public void checkDeckWithOnlyTwoOfs() {

        // Setup deck deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = new ArrayList<>();
        final List<Integer> twoOfIds = Arrays.asList(374, 2042, 734, 1927, 2275, 2901, 39169, 39715, 40407, 40496,
                41243, 41496, 41524, 41927, 43502);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        // Setup expected validation result.
        final List<String> message = new ArrayList<String>();
        final ValidationResult expectedValidationResult = new ValidationResult();
        expectedValidationResult.setValidationStatus(ValidationStatus.VALID);
        expectedValidationResult.setMessage(message);

        // Run test
        final ValidationResult actualValidationResult = deckValidationService.validateDeck(deck);

        // Assert
        Assert.assertEquals(expectedValidationResult, actualValidationResult);

    }

    /**
     * Validation result when a deck contains a hero.
     */
    @Test
    public void checkDeckWithHero() {

        // Setup deck deck.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(7, 2042, 2949, 43419);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927, 43502);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET)
                    .withName("Garrosh");
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id)
                    .withCardClass(StartingHero.JAINA.getHeroClass()).withRarity(CARD_RARITY).withSet(CARD_SET);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        // Setup expected validation result.
        final List<String> message = new ArrayList<String>();
        message.add(String.format(ErrorDescriptionConstants.HERO_IN_DECK, "Garrosh"));
        final ValidationResult expectedValidationResult = new ValidationResult();
        expectedValidationResult.setValidationStatus(ValidationStatus.INVALID);
        expectedValidationResult.setMessage(message);

        // Run test
        final ValidationResult actualValidationResult = deckValidationService.validateDeck(deck);

        // Assert
        Assert.assertEquals(expectedValidationResult, actualValidationResult);

    }

}
