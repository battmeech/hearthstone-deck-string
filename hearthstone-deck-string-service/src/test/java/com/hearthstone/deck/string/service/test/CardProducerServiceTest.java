/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service.test;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.hearthstone.deck.string.config.HearthstoneDeckStringConfig;
import com.hearthstone.deck.string.config.HearthstoneJsonMethod;
import com.hearthstone.deck.string.service.CardProducerService;
import com.hearthstone.deck.string.service.CardProducerServiceHandler;

/**
 * Unit test for {@link CardProcuerServiceHandler}
 */
@RunWith(MockitoJUnitRunner.class)
public class CardProducerServiceTest {

    /** The URL used for this test. */
    private static final String URL = "http://api.hearthstonejson.com/v1/latest/enUS/cards.collectible.json";

    /** The hearthstone deck string config. */
    @Mock
    private HearthstoneDeckStringConfig hearthstoneDeckStringConfig;

    /** Class under test */
    private CardProducerService hearthstoneJsonProducerService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        this.hearthstoneJsonProducerService = new CardProducerServiceHandler(hearthstoneDeckStringConfig);
    }

    /**
     * Run the service to the URL.
     */
    @Test
    public void runToUrl() {

        // Mock
        Mockito.when(hearthstoneDeckStringConfig.getJsonMethod()).thenReturn(HearthstoneJsonMethod.URL);
        Mockito.when(hearthstoneDeckStringConfig.getJsonUrl()).thenReturn(URL);

        // Run test
        hearthstoneJsonProducerService.produceCards();

        // Verify
        Mockito.verify(hearthstoneDeckStringConfig).getJsonMethod();
        Mockito.verify(hearthstoneDeckStringConfig).getJsonUrl();
    }

    /**
     * Run the service to the file.
     */
    @Test
    public void runToFile() {

        final File file = new File(getClass().getClassLoader().getResource("hearthstone.json").getPath());

        final String filePath = file.getAbsolutePath();

        // Mock
        Mockito.when(hearthstoneDeckStringConfig.getJsonMethod()).thenReturn(HearthstoneJsonMethod.FILE);
        Mockito.when(hearthstoneDeckStringConfig.getJsonFileLocation()).thenReturn(filePath);

        // Run test
        hearthstoneJsonProducerService.produceCards();

        // Verify
        Mockito.verify(hearthstoneDeckStringConfig).getJsonMethod();
        Mockito.verify(hearthstoneDeckStringConfig).getJsonFileLocation();
    }

}
