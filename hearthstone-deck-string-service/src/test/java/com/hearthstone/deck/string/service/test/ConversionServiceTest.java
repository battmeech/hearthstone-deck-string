/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Card.CardBuilder;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Deck.DeckBuilder;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;
import com.hearthstone.deck.string.service.CardEnrichmentService;
import com.hearthstone.deck.string.service.CardProducerService;
import com.hearthstone.deck.string.service.ConversionService;
import com.hearthstone.deck.string.service.ConversionServiceHandler;

/**
 * Unit testing for {@link ConversionService}
 */
@RunWith(MockitoJUnitRunner.class)
public class ConversionServiceTest {

    /** The Constant deck string. */
    private static final String DECK_STRING = "AAEBAf0EBPYC+g+FF5vTAg3eBYcP4xHVFoGyAqO2Ate7ArC8ApvCApjEArTEAsfHAu7TAgA=";

    /** Card Reader Service. */
    @Mock
    private CardEnrichmentService cardReaderService;

    /** The card producer service. */
    @Mock
    private CardProducerService cardProducerService;

    /** Class under test. */
    private ConversionService conversionService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        this.conversionService = new ConversionServiceHandler(cardReaderService, cardProducerService);
    }

    /**
     * Test the decode method.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Test
    public void testDecode() throws IOException {

        // Setup expected deck.
        final StartingHero hero = StartingHero.JAINA;

        final Card cards[] = null;

        final Format format = Format.FT_WILD;

        // Expected Card is Dr Boom.
        final CardBuilder cardBuilder = new CardBuilder().withCardClass("neutral").withName("Dr. Boom").withDbfId(100);
        final Card card = cardBuilder.build();

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (int index = 0; index < 4; index++) {
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (int index = 0; index < 13; index++) {
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck expectedDeck = deckBuilder.build();

        // Mock
        Mockito.when(cardProducerService.produceCards()).thenReturn(cards);
        Mockito.when(cardReaderService.findCardInJson(Mockito.anyInt(), Mockito.anyObject())).thenReturn(card);

        // Perform test
        final Deck actualDeck = conversionService.decodeFromString(DECK_STRING);

        // Assert
        Assert.assertEquals(expectedDeck, actualDeck);

        // Verify
        Mockito.verify(cardProducerService).produceCards();
        Mockito.verify(cardReaderService, Mockito.times(17)).findCardInJson(Mockito.anyInt(), Mockito.anyObject());
    }

    /**
     * Test the encode method.
     */
    @Test
    public void testEncode() {

        // Setup deck to be encoded.
        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final List<Integer> oneOfIds = Arrays.asList(374, 2042, 2949, 43419);
        final List<Integer> twoOfIds = Arrays.asList(734, 1927, 2275, 2901, 39169, 39715, 40407, 40496, 41243, 41496,
                41524, 41927, 43502);

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (final Integer id : oneOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id);
            final Card card = cardBuilder.build();
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (final Integer id : twoOfIds) {
            final CardBuilder cardBuilder = new CardBuilder().withDbfId(id);
            final Card card = cardBuilder.build();
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deck = deckBuilder.build();

        // Perform test
        final String deckString = conversionService.convertDeckToDeckString(deck);

        // Assert
        Assert.assertEquals(deckString, DECK_STRING);
    }
}
