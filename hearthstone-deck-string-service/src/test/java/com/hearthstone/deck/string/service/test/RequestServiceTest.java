/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.hearthstone.deck.string.config.HearthstoneDeckStringConfig;
import com.hearthstone.deck.string.config.HearthstoneDeckStringRequestContext;
import com.hearthstone.deck.string.config.exception.InvalidDeckException;
import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Card.CardBuilder;
import com.hearthstone.deck.string.domain.Deck;
import com.hearthstone.deck.string.domain.Deck.DeckBuilder;
import com.hearthstone.deck.string.domain.Format;
import com.hearthstone.deck.string.domain.StartingHero;
import com.hearthstone.deck.string.domain.ValidationResult;
import com.hearthstone.deck.string.domain.ValidationStatus;
import com.hearthstone.deck.string.service.ConversionService;
import com.hearthstone.deck.string.service.RandomDeckService;
import com.hearthstone.deck.string.service.RequestService;
import com.hearthstone.deck.string.service.RequestServiceHandler;
import com.hearthstone.deck.string.service.validation.DeckValidationService;

/**
 * Unit test for {@link RequestService}
 */
@RunWith(MockitoJUnitRunner.class)
public class RequestServiceTest {

    /** The conversion service. */
    @Mock
    private ConversionService conversionService;

    /** The deck validation service. */
    @Mock
    private DeckValidationService deckValidationService;

    /** The hearthstone deck string request context. */
    @Mock
    private HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext;

    /** The hearthstone deck string config. */
    @Mock
    private HearthstoneDeckStringConfig hearthstoneDeckStringConfig;

    /** The random deck service. */
    @Mock
    private RandomDeckService randomDeckService;

    /** The request service. */
    private RequestService requestService;

    @Before
    public void setup() {
        this.requestService = new RequestServiceHandler(conversionService, deckValidationService,
                hearthstoneDeckStringRequestContext, hearthstoneDeckStringConfig, randomDeckService);
    }

    /**
     * Test valid deck string.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Test
    public void testValidDeckString() throws IOException {

        // Setup
        final String deckString = "Definitely a deck string";

        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final CardBuilder cardBuilder = new CardBuilder().withCardClass("neutral").withName("Dr. Boom").withDbfId(100);
        final Card card = cardBuilder.build();

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (int index = 0; index < 4; index++) {
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (int index = 0; index < 13; index++) {
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck expectedDeck = deckBuilder.build();

        final ValidationResult validationResult = new ValidationResult();
        validationResult.setValidationStatus(ValidationStatus.VALID);

        // Mock
        Mockito.when(hearthstoneDeckStringConfig.toValidate()).thenReturn(true);
        Mockito.when(conversionService.decodeFromString(deckString)).thenReturn(expectedDeck);
        Mockito.when(deckValidationService.validateDeck(expectedDeck)).thenReturn(validationResult);
        Mockito.when(hearthstoneDeckStringRequestContext.getUniqueRequestReference()).thenReturn("Unique");

        // Run
        final Deck actualDeck = requestService.processDeckString(deckString);

        // Assert
        Assert.assertEquals(expectedDeck, actualDeck);

        // Verify
        Mockito.verify(conversionService).decodeFromString(deckString);
        Mockito.verify(deckValidationService).validateDeck(expectedDeck);
        Mockito.verify(hearthstoneDeckStringConfig).toValidate();
    }

    /**
     * Test valid deck.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Test
    public void testValidDeck() throws IOException {

        // Setup
        final String expectedDeckString = "Definitely a deck string";

        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final CardBuilder cardBuilder = new CardBuilder().withCardClass("neutral").withName("Dr. Boom").withDbfId(100);
        final Card card = cardBuilder.build();

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (int index = 0; index < 4; index++) {
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (int index = 0; index < 13; index++) {
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deckToTest = deckBuilder.build();

        final ValidationResult validationResult = new ValidationResult();
        validationResult.setValidationStatus(ValidationStatus.VALID);

        // Mock
        Mockito.when(hearthstoneDeckStringConfig.toValidate()).thenReturn(true);
        Mockito.when(conversionService.convertDeckToDeckString(deckToTest)).thenReturn(expectedDeckString);
        Mockito.when(deckValidationService.validateDeck(deckToTest)).thenReturn(validationResult);

        // Run
        final String actualDeckString = requestService.processDeck(deckToTest);

        // Assert
        Assert.assertEquals(expectedDeckString, actualDeckString);

        // Verify
        Mockito.verify(conversionService).convertDeckToDeckString(deckToTest);
        Mockito.verify(deckValidationService).validateDeck(deckToTest);
        Mockito.verify(hearthstoneDeckStringConfig).toValidate();
    }

    /**
     * Test valid deck.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Test(expected = InvalidDeckException.class)
    public void testInvalidDeck() throws IOException {

        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final CardBuilder cardBuilder = new CardBuilder().withCardClass("neutral").withName("Dr. Boom").withDbfId(100);
        final Card card = cardBuilder.build();

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (int index = 0; index < 4; index++) {
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (int index = 0; index < 13; index++) {
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deckToTest = deckBuilder.build();

        final List<String> validationMessages = new ArrayList<String>();
        validationMessages.add("Stop");
        validationMessages.add("Putting");
        validationMessages.add("Bad");
        validationMessages.add("Decks");
        validationMessages.add("In");

        final ValidationResult validationResult = new ValidationResult();
        validationResult.setValidationStatus(ValidationStatus.INVALID);
        validationResult.setMessage(validationMessages);

        // Mock
        Mockito.when(hearthstoneDeckStringConfig.toValidate()).thenReturn(true);
        Mockito.when(deckValidationService.validateDeck(deckToTest)).thenReturn(validationResult);

        // Run
        requestService.processDeck(deckToTest);

        // Assert
        Assert.fail();

        // Verify
        Mockito.verify(hearthstoneDeckStringConfig).toValidate();
        Mockito.verify(deckValidationService).validateDeck(deckToTest);
    }

    /**
     * Test invalid deck string
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Test(expected = InvalidDeckException.class)
    public void testInvalidDeckString() throws IOException {

        final String deckStringToTest = "Legit deck, probably secret paladin";

        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final CardBuilder cardBuilder = new CardBuilder().withCardClass("neutral").withName("Dr. Boom").withDbfId(100);
        final Card card = cardBuilder.build();

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (int index = 0; index < 4; index++) {
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (int index = 0; index < 13; index++) {
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deckToTest = deckBuilder.build();

        final List<String> validationMessages = new ArrayList<String>();
        validationMessages.add("Stop");
        validationMessages.add("Putting");
        validationMessages.add("Bad");
        validationMessages.add("Decks");
        validationMessages.add("In");

        final ValidationResult validationResult = new ValidationResult();
        validationResult.setValidationStatus(ValidationStatus.INVALID);
        validationResult.setMessage(validationMessages);

        // Mock
        Mockito.when(hearthstoneDeckStringConfig.toValidate()).thenReturn(true);
        Mockito.when(conversionService.decodeFromString(deckStringToTest)).thenReturn(deckToTest);
        Mockito.when(deckValidationService.validateDeck(deckToTest)).thenReturn(validationResult);

        // Run
        requestService.processDeckString(deckStringToTest);

        // Assert
        Assert.fail();

        // Verify
        Mockito.verify(deckValidationService).validateDeck(deckToTest);
        Mockito.verify(conversionService).decodeFromString(deckStringToTest);
        Mockito.verify(hearthstoneDeckStringConfig).toValidate();
    }

    /**
     * Test validation skip on deck string.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Test
    public void testValidationSkipDeckString() throws IOException {

        // Setup
        final String deckString = "Definitely a deck string";

        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final CardBuilder cardBuilder = new CardBuilder().withCardClass("neutral").withName("Dr. Boom").withDbfId(100);
        final Card card = cardBuilder.build();

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (int index = 0; index < 4; index++) {
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (int index = 0; index < 13; index++) {
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck expectedDeck = deckBuilder.build();

        final ValidationResult validationResult = new ValidationResult();
        validationResult.setValidationStatus(ValidationStatus.VALID);

        // Mock
        Mockito.when(hearthstoneDeckStringConfig.toValidate()).thenReturn(false);
        Mockito.when(conversionService.decodeFromString(deckString)).thenReturn(expectedDeck);
        Mockito.when(hearthstoneDeckStringRequestContext.getUniqueRequestReference()).thenReturn("Unique");

        // Run
        final Deck actualDeck = requestService.processDeckString(deckString);

        // Assert
        Assert.assertEquals(expectedDeck, actualDeck);

        // Verify
        Mockito.verify(conversionService).decodeFromString(deckString);
        Mockito.verify(hearthstoneDeckStringConfig).toValidate();
        Mockito.verifyZeroInteractions(deckValidationService);
    }

    /**
     * Test valididation skip on deck.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Test
    public void testValididationSkipDeck() throws IOException {

        // Setup
        final String expectedDeckString = "Definitely a deck string";

        final StartingHero hero = StartingHero.JAINA;

        final Format format = Format.FT_WILD;

        final CardBuilder cardBuilder = new CardBuilder().withCardClass("neutral").withName("Dr. Boom").withDbfId(100);
        final Card card = cardBuilder.build();

        final List<Card> oneOfCards = new ArrayList<Card>();
        for (int index = 0; index < 4; index++) {
            oneOfCards.add(card);
        }

        final List<Card> twoOfCards = new ArrayList<Card>();
        for (int index = 0; index < 13; index++) {
            twoOfCards.add(card);
        }

        final DeckBuilder deckBuilder = new DeckBuilder().withFormat(format).withStartingHero(hero)
                .withOneOfCards(oneOfCards).withTwoOfCards(twoOfCards);
        final Deck deckToTest = deckBuilder.build();

        final ValidationResult validationResult = new ValidationResult();
        validationResult.setValidationStatus(ValidationStatus.VALID);

        // Mock
        Mockito.when(hearthstoneDeckStringConfig.toValidate()).thenReturn(false);
        Mockito.when(conversionService.convertDeckToDeckString(deckToTest)).thenReturn(expectedDeckString);

        // Run
        final String actualDeckString = requestService.processDeck(deckToTest);

        // Assert
        Assert.assertEquals(expectedDeckString, actualDeckString);

        // Verify
        Mockito.verify(conversionService).convertDeckToDeckString(deckToTest);
        Mockito.verify(hearthstoneDeckStringConfig).toValidate();
        Mockito.verifyZeroInteractions(deckValidationService);
    }

}
