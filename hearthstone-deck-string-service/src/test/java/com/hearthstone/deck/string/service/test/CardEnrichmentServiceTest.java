/**
 * Matt Beech 2018
 */
package com.hearthstone.deck.string.service.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hearthstone.deck.string.config.HearthstoneDeckStringRequestContext;
import com.hearthstone.deck.string.domain.Card;
import com.hearthstone.deck.string.domain.Card.CardBuilder;
import com.hearthstone.deck.string.service.CardEnrichmentService;
import com.hearthstone.deck.string.service.CardEnrichmentServiceHandler;

/**
 * Unit test for {@link CardEnrichmentService}
 */
@RunWith(MockitoJUnitRunner.class)
public class CardEnrichmentServiceTest {

    /** The logger. */
    private static final Logger LOG = LoggerFactory.getLogger(CardEnrichmentServiceTest.class);

    /** Card name used for testing. */
    private static final String CARD_NAME = "Fireball";

    /** Card ID used for testing. */
    private static final int CARD_ID = 315;

    /** Card class used for testing. */
    private static final String CARD_CLASS = "MAGE";

    /** The card rarity for testing. */
    private static final String CARD_RARITY = "FREE";

    /** The expansion for testing. */
    private static final String CARD_SET = "CORE";

    /** The hearthstone deck string request context. */
    @Mock
    private HearthstoneDeckStringRequestContext hearthstoneDeckStringRequestContext;

    /** Class under test. */
    private CardEnrichmentService cardEnrichmentService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        cardEnrichmentService = new CardEnrichmentServiceHandler(hearthstoneDeckStringRequestContext);
    }

    /**
     * Test card enrichment.
     */
    @Test
    public void testCardEnrichment() {

        // Set up the expected card
        final CardBuilder cardBuilder = new CardBuilder().withCardClass(CARD_CLASS).withName(CARD_NAME)
                .withDbfId(CARD_ID).withRarity(CARD_RARITY).withSet(CARD_SET);
        final Card expectedCard = cardBuilder.build();
        final Card cards[] = { expectedCard };

        // Perform test
        final Card actualCard = cardEnrichmentService.findCardInJson(CARD_ID, cards);
        LOG.debug(actualCard.toString());

        // Assert
        Assert.assertEquals(expectedCard, actualCard);
    }
}
